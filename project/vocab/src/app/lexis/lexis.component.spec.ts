import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LexisComponent } from './lexis.component';

describe('LexisComponent', () => {
  let component: LexisComponent;
  let fixture: ComponentFixture<LexisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LexisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LexisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
