import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VocabListsComponent } from './vocab-lists/vocab-lists.component';
import { NewGslComponent } from './new-gsl/new-gsl.component';
import { LexisComponent } from './lexis/lexis.component';
import { MorphologyComponent } from './morphology/morphology.component';
import { SyntaxComponent } from './syntax/syntax.component';
import { SearchComponent } from './search/search.component';
import {AnalysisComponent} from './analysis/analysis.component';

const routes: Routes = [
	{'path': 'new-gsl', 'component': NewGslComponent},
	{'path': 'lexis', 'component': LexisComponent},
	{'path': 'morphology', 'component': MorphologyComponent},
	{'path': 'syntax', 'component': SyntaxComponent},
	{'path': 'vocab-lists', 'component': VocabListsComponent},
	{'path': 'search', 'component': SearchComponent},
	{'path': 'analysis', 'component': AnalysisComponent},
	{'path': '', 'redirectTo': '/vocab-lists', 'pathMatch': 'full'}
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
