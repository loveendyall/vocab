<?php
	include 'functions.php';


	if(!empty($_GET['c'])){
		$file = 'docs/new_GSL_semantics.txt';
		$pos = htmlspecialchars($_GET['p']);
		$usas = htmlspecialchars($_GET['u']);

		$my_categories= display_categories($pos, $usas);
		echo JSON_encode(array("Semantic category" => $my_categories[1], "Word class" => $my_categories[0], "results" => search_for_categories($pos, $usas, $file)));
	} else {
		$file = 'docs/new_GSL.txt';
		$searchfor = htmlspecialchars($_GET['q']);
		echo JSON_encode(search($searchfor, $file));
	}
?>