<?php
function header_page(){
	echo '<div id="header">
		<!-- Header start -->
		<div align ="left">
		<ul class="menu">
			<li><a href="index.php">New-GSL</a></li>
			<li><a href="browse.php">Browse</a></li>
			<li><a href="analyse.php">Analyse</a></li>
			<li><a href="discuss.php">Discuss</a></li>
		</ul>
		</div>
		<!-- HTML for SEARCH BAR -->
		<div align ="right">
		<form method="get" action="search.php">
		        <input type="text" class="textinput" name="q" size="21" maxlength="120"><input type="submit" value="search new-GSL" class="button">
		</form>
		</div>
		<!-- Header end -->
	</div>';
}

function footer (){
	echo '(c) Vaclav Brezina 2014, <a href="http://cass.lancs.ac.uk/">CASS, Lancaster University';
}

function search ($searchfor, $file){

	if (!empty($searchfor)) 
	{

	// the following line prevents the browser from parsing this as HTML.
	//header('Content-Type: text/plain');

	// get the file contents, assuming the file to be readable (and exist)
	$contents = file_get_contents($file);
	// escape special characters in the query
	$pattern = preg_quote($searchfor, '/');
	// finalise the regular expression, matching the whole line
	$pattern = "/^$pattern.*\$/i m";
	// search, and store all matching occurences in $matches
	if(preg_match_all($pattern, $contents, $matches)){
	   echo implode("</p>", $matches[0]);
	}
	else
	{
	   echo "No matches found.";
	}
	}
	else echo "No search term entered.";
}


//analyse functions start here





function count_POS_in_text_and_get_items($text_array, $pos){
	
	// create an subset of the text array based on the $pos (part_of_speech)
	$text_array_subset = array();
	foreach ($text_array as $key => $value) {
		if (preg_match("/^".$pos."_/",$key)){
			$text_array_subset[$key] = $value;
		}
	}
}	
	
function loglik(){
}

function cohen_d($master_list, $reference_tokens, $tokens){
	foreach ($master_list as $key => &$value){
	$norm_corpus = $value[0]/$tokens; 
	$norm_ref = $value[1]/$reference_tokens;
	$value_cohen = ($norm_corpus - $norm_ref)/$value[2];
	$value_cohen_abs = abs($value_cohen); 
	if ($value_cohen < 0)
		$pos_or_neg = "-";
	elseif ($value_cohen > 0)
		$pos_or_neg = "+";
	elseif ($value_cohen == 0)
		$pos_or_neg = "N/A";
	// data format: (occurrence in corpus, per cent in corpus, occurrence in reference corp., percent in reference corp, cohen's d  
	$value =  array($master_list[$key][0], $norm_corpus*100, $master_list[$key][1], $norm_ref*100, $value_cohen_abs, $pos_or_neg); 
	}
	
	return $master_list;
}


function get_keywords($reference_corpus, $reference_tokens, $corpus, $tokens, $statistic){

	//create a master wordlist
	$master_list = array();
	foreach ($corpus as $key => $value){
			if (array_key_exists($key, $reference_corpus)){
				$master_list[$key] = array($value, $reference_corpus[$key][0], $reference_corpus[$key][1]);
			}
			else{
			$master_list[$key] = array($value, 0, 0.5);
			}
	}
	foreach ($reference_corpus as $key => $value){
			if (!array_key_exists($key, $master_list)){
			$master_list[$key] = array(0, $reference_corpus[$key][0], $reference_corpus[$key][1]);
			}
	}

	if ($statistic == "cohen_d"){
	$master_list = cohen_d($master_list, $reference_tokens, $tokens);
	}
	//sort according to statistic
	uasort($master_list, "cmp");
	//add rank value
	$i =1;
	foreach ($master_list as $key => &$value){
		$value =  array($i, $master_list[$key][0], $master_list[$key][1], $master_list[$key][2], $master_list[$key][3],$master_list[$key][4], $master_list[$key][5]); 
		$i++;
	}
	return $master_list;

}	



function cmp($a, $b) {
        return $b[4] - $a[4];
}

function polish($string) {
		$string=preg_replace('/_[A-Z]+$/','',$string);
		$string=preg_replace('/(^[A-Z]+)_N$/','<strong><font color = "blue">\1</font></strong>',$string);
		$string=preg_replace('/(^[A-Z]+)_Y$/','<strong><font color = "navy">\1</font></strong>',$string);
		$string=preg_replace('/(^[A-Z]+)_V$/','<strong><font color = "red">\1</font></strong>',$string);
		$string=preg_replace('/(^[A-Z]+)_M$/','<strong><font color = "maroon ">\1</font></strong>',$string);
		$string=preg_replace('/(^[A-Z]+)_J$/','<strong><font color = "lime">\1</font></strong>',$string);
		$string=preg_replace('/(^[A-Z]+)_R$/','<strong><font color = "green">\1</font></strong>',$string);
		$string=preg_replace('/(^[A-Z]+)_X$/','<strong><font color = "black">\1</font></strong>',$string);
		$string=preg_replace('/(^[A-Z]+)_C$/','<strong><font color = "orange">\1</font></strong>',$string);
		$string=preg_replace('/(^[A-Z]+)_P$/','<strong><font color = "purple">\1</font></strong>',$string);
		$string=preg_replace('/(^[A-Z]+)_W$/','<strong><font color = "darkgoldenrod">\1</font></strong>',$string);
	return $string;
}

function array_change_key(&$array, $old_key, $new_key)
{
    $array[$new_key] = $array[$old_key];
    unset($array[$old_key]);
    return;
}
function loop_through_spelling_variants($array, $spelling_array){
	
	
	foreach ($spelling_array as $value){
		$array = identify_spelling_differences($array, $value[0], $value[1]);
		}
	uasort($array, "cmp");
	return $array;
}
function identify_spelling_differences($array, $variant1, $variant2){
	$variant1=strtoupper($variant1);
	$variant2=strtoupper($variant2);
	foreach ($array as $key => $value) {
		if (preg_match("/^".$variant1."_[A-Z]_.*/",$key)){
			$var1_rank = $value[0];
			if ($value[6] == "+")
				$var1_pos_or_neg =1;
			elseif ($value[6] == "-")
				$var1_pos_or_neg =0;
		
		}
	}

	foreach ($array as $key => $value) {
		if (preg_match("/^".$variant2."_[A-Z]_.*/",$key)){
			$var2_rank = $value[0];
			if ($value[6] == "+")
				$var2_pos_or_neg =1;
			elseif ($value[6] == "-")
				$var2_pos_or_neg =0;
		
		}
	}
	
	if(isset($var1_rank, $var2_rank) && abs($var1_rank-$var2_rank)<20){
		foreach ($array as $key => $value) {
			if (preg_match("/^".$variant1."_[A-Z]_.*/",$key)){
			$key1 = preg_replace("/(^".$variant1."_)[A-Z](_.*)/","$1W$2",$key);
			array_change_key($array, $key, $key1);
			}
		}

		foreach ($array as $key => $value) {
			if (preg_match("/^".$variant2."_[A-Z]_.*/",$key)){
			$key1 = preg_replace("/(^".$variant2."_)[A-Z](_.*)/","$1W$2",$key);
			array_change_key($array, $key, $key1);
			}
		}
		
	}
	
return $array;
}


function get_sublist($array, $category, $neg_pos){
// create an subset of the array based on the $category & $value
	$text_array_subset = array();
	foreach ($array as $key => $value) {
		if (preg_match("/^.*_".$category."_.*/",$key)&& preg_match("/".$neg_pos."/",$value[6])){
			$text_array_subset[$key] = $value;
		}
	}
	$string="";
	foreach ($text_array_subset as $key => $value) {
		if(!empty($string))
			$string = $string.", ".polish($key)." (".$value[0].")";
		else
			$string = polish($key)." (".$value[0].")";
	}
	
return $string;
}

function process($f) {
	$f= str_replace("</s>","",$f);
	$f= str_replace("<s>","",$f);
	$f= str_replace("'","",$f);
	$f= str_replace("’","",$f);
	$f= preg_replace('/,\t,\t,/','',$f);
	$f=strtoupper($f);
	
	//$f= preg_replace('/(^[A-Z]?([a-z]*)?|^\'?[a-z]|^[0-9]*)\t/','',$f);
	$f= preg_replace('/\.\tSENT\t\./','',$f);//delete full stops
	$f= preg_replace('/^[0-9]+\tCD\t.*/','',$f);//delete numbers
	$f= preg_replace('/\t/','_',$f);
	

	
	//$f= preg_replace('/^[A-Z]+_VBG_accord/','C_according',$f);//according to
	
	$f= preg_replace('/(^[A-Z]+_)NP_/','\1Y_',$f);//proper nouns
	$f= preg_replace('/(^[A-Z]+_)N[A-Z]*_/','\1N_',$f);//nouns
	$f= preg_replace('/(^[A-Z]+_)JJ[A-Z]?_/','\1J_',$f);//adjectives
	$f= preg_replace('/(^[A-Z]+_)V[A-Z]*_/','\1V_',$f);//verbs
	$f= preg_replace('/(^[A-Z]+_)MD_/','\1M_',$f);//modals
	$f= preg_replace('/(^[A-Z]+_)PP\$?_/','\1P_',$f);//pronouns
	$f= preg_replace('/(^[A-Z]+_)WP\$?_/','\1P_',$f);//pronouns
	$f= preg_replace('/(^[A-Z]+_)WDT_/','\1P_',$f);//pronouns
	$f= preg_replace('/(^[A-Z]+_)IN_/','\1C_',$f);//prep/conjunctions
	$f= preg_replace('/(^[A-Z]+_)CC_/','\1C_',$f);//prep/conjunctions
	$f= preg_replace('/(^[A-Z]+_)CD_/','\1X_',$f);//numerals
	$f= preg_replace('/(^[A-Z]+_)DT_/','\1X_',$f);//determiners
	$f= preg_replace('/(^[A-Z]+_)EX_/','\1X_',$f);//existential there
	$f= preg_replace('/(^[A-Z]+_)PDT_/','\1X_',$f);//predeterminer
	$f= preg_replace('/(^[A-Z]+_)RP_/','\1X_',$f);//particle
	$f= preg_replace('/(^[A-Z]+_)RB[A-Z]*_/','\1R_',$f);//adverb
	$f= preg_replace('/(^[A-Z]+_)WRB_/','\1R_',$f);//adverb
	$f= preg_replace('/(^[A-Z]+_)TO_/','\1X_',$f);//to particle
	$f= preg_replace('/(^[A-Z]+_)UH_/','\1X_',$f);//interjections
	$f= preg_replace('/(^[A-Z]_)POS_/','\1X_',$f);//possessive
	
	$f= preg_replace('/(^[A-Z]+_)R_yes/','\1X_yes',$f);//interjections
	$f= preg_replace('/(^[A-Z]+_)R_no/','\1X_no',$f);//interjections
	
	$f= preg_replace('/(^[A-Z]+_)X_an/','\1X_a',$f);//article
	
	$f= preg_replace('/(^[A-Z]+_)R_n\'t/','\1R_not',$f);//not - lumping
	$f= preg_replace('/(^[A-Z]+_)R_nt/','\1R_not',$f);//not - lumping
	$f= preg_replace('/(^[A-Z]+_)X_not/','\1R_not',$f);//not - lumping
	
	$f= preg_replace('/(^[A-Z]+_)N_lot/','\1X_lot',$f);//lot of -correction
	
	$f= preg_replace('/(^[A-Z]+_)P_me/','\1P_I',$f);//pron - lumping
	$f= preg_replace('/(^[A-Z]+_)P_him/','\1P_he',$f);//pron - lumping
	$f= preg_replace('/(^[A-Z]+_)P_her/','\1P_she',$f);//pron - lumping
	$f= preg_replace('/(^[A-Z]+_)P_us/','\1P_we',$f);//pron - lumping
	$f= preg_replace('/(^[A-Z]+_)P_them/','\1P_they',$f);//pron - lumping
	$f= preg_replace('/(^[A-Z]+_)P_them/','\1P_they',$f);//pron - lumping

	$f= preg_replace('/(^[A-Z]+_)JJ_mine/','\1P_my',$f);//pron-correction
	$f= preg_replace('/(^[A-Z]+_)P_yours/','\1P_your',$f);//pron - lumping
	$f= preg_replace('/(^[A-Z]+_)P_hers/','\1P_her',$f);//pron - lumping
	$f= preg_replace('/(^[A-Z]+_)P_ours/','\1P_our',$f);//pron - lumping
	$f= preg_replace('/(^[A-Z]+_)P_theirs/','\1P_their',$f);//pron - lumping

	$f= preg_replace('/(^[A-Z]+_)[A-Z]+_somebody/','\1P_somebody',$f);//pron - lumping
	$f= preg_replace('/(^[A-Z]+_)[A-Z]+_someone/','\1P_someone',$f);//pron - lumping
	$f= preg_replace('/(^[A-Z]+_)[A-Z]+_something/','\1P_something',$f);//pron - lumping

	$f= preg_replace('/(^[A-Z]+_)[A-Z]+_anybody/','\1P_anybody',$f);//pron - lumping
	$f= preg_replace('/(^[A-Z]+_)[A-Z]+_anyone/','\1P_anyone',$f);//pron - lumping
	$f= preg_replace('/(^[A-Z]+_)[A-Z]+_anything/','\1P_anything',$f);//pron - lumping
	
	$f= preg_replace('/(^[A-Z]+_)[A-Z]+_nobody/','\1P_nobody',$f);//pron - lumping
	$f= preg_replace('/(^[A-Z]+_)[A-Z]+_nothing/','\1P_nothing',$f);//pron - lumping
	
	
	$f= preg_replace('/(^[A-Z]+_)N_Mr/','\1X_Mr',$f);//abbreviations - lumping
	$f= preg_replace('/(^[A-Z]+_)N_Mr./','\1X_Mr',$f);//abbreviations - lumping
	$f= preg_replace('/(^[A-Z]+_)N_Mrs/','\1X_Mrs',$f);//abbreviations - lumping
	$f= preg_replace('/(^[A-Z]+_)N_Mrs./','\1X_Mrs',$f);//abbreviations - lumping
	$f= preg_replace('/(^[A-Z]+_)N_Dr/','\1X_Dr',$f);//abbreviations - lumping
	$f= preg_replace('/(^[A-Z]+_)N_Dr./','\1X_Dr',$f);//abbreviations - lumping
	
	$f= preg_replace('/(^[A-Z]+_)J_first/','\1X_first',$f);//numbers - lumping
	$f= preg_replace('/(^[A-Z]+_)J_second/','\1X_second',$f);//numbers - lumping
	$f= preg_replace('/(^[A-Z]+_)J_third/','\1X_third',$f);//numbers - lumping
	$f= preg_replace('/(^[A-Z]+_)N_eleven/','\1X_eleven',$f);//numbers - lumping
	$f= preg_replace('/(^[A-Z]+_)N_twelve/','\1X_twelve',$f);//numbers - lumping
	
	$f= preg_replace('/(^[A-Z]+_)R_south/','\1N_South',$f);//directions
	$f= preg_replace('/(^[A-Z]+_)[A-Z]_north/','\1N_North',$f);//directions
	$f= preg_replace('/(^[A-Z]+_)[A-Z]_west/','\1N_West',$f);//directions
	$f= preg_replace('/(^[A-Z]+_)[A-Z]_east/','\1N_East',$f);//directions
	
	
	return $f;
	
}

//analysis display functions
function display_pos_in_text($array) {
	$new_array = $array[1];
	foreach ($new_array as $key => $value) {
		$new_array[$key] = preg_replace("/^[A-Z]_/", "", $value);	
		
		}
	
	
	$words_string = implode(", ",$new_array);
	return $words_string;
}

//analysis display functions
function display_individual_items($array) {
	
	foreach ($array as $key => $value) {
		$new_array[$key] = preg_replace("/^[A-Z]_/", "", $value);	
		
		}
	
	sort($new_array);
	$words_string = implode(", ",$new_array);
	return $words_string;
}



?>