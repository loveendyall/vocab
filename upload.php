<?php
session_start();
include 'CODE/functions_morph.php';
include 'CODE/functions.php';
//print_r($_FILES);
$error = "";
if (isset($_FILES["fileToUpload"])) {
	$allowedExts = array("csv");
	$temp = explode(".", $_FILES["fileToUpload"]["name"]);
	$extension = end($temp);

	if ($_FILES["fileToUpload"]["error"] > 0) {
		$error .= "Error opening the file<br />";
	}
	//if ( $_FILES["fileToUpload"]["type"] != "text/plain") {	
		//$error .= "Mime type not allowed<br />";
	//}
	if (!in_array($extension, $allowedExts)) {
		$error .= "Extension not allowed<br />";
	}
	if ($_FILES["fileToUpload"]["size"] > 512000) {
		$error .= "File size should be less than 100 kB<br/>";
	}

	if ($error == "") {
		;
	} 
	else {
		echo $error;
		exit;
	}
}	
else{
	echo 'ERROR: File error';
	exit;
}
/*
if (!isset ($_SESSION['i'])){
	echo "ERROR: Your input could not be processed because the request was badly-formed, please go back and try again.";
	exit;
}
*/
//define settings
//$language = $_SESSION['i'][0];
//$proper = $_SESSION['i'][1];
//$periphrastic = $_SESSION['i'][2];

// store file content as a string in $str
$str = file_get_contents($_FILES["fileToUpload"]["tmp_name"]);

$str = str_replace("\r\n", "\n", $str);

$str = ltrim($str, 'Word_form,POS Headword,V_Exponent,N_Exponent');
$str = rtrim($str, '\n');
//$str = rtrim($str, 'SOURCE: Morphological analysis tool; Vaclav Brezina & Gabriele Pallotti 2015');
//echo $str
$text = explode("\n", $str);



foreach ($text as &$value){
$value = preg_replace("/\",\"/","xxcxx", $value );
//$value = preg_replace("/,$/","", $value );
$value = preg_replace("/,/","\t", $value );
$value = preg_replace("/xxcxx/",",", $value );
$value = preg_replace("/SOURCE: Morphological analysis tool; Vaclav Brezina & Gabriele Pallotti 2015/","", $value);
}
//print_r($text);


$morphology_V = array();
$morphology_N = array();


unlink($_FILES["fileToUpload"]["tmp_name"]);
		
		
//populate morphology arrays

//verbs
foreach($text as &$w){	
	if (preg_match('/^.+?\t[MV].*[^#]\t.+?\t.+?\s?$/', $w)){
		$w1 = preg_replace('/^(.+?\t[VM].+?\t.+?\t)(.+?)$/','$2', $w);
		$morphology_V[] = trim($w1);
	}
}

//nouns - proper nouns match the pattern only if the morpheme was added earlier 
foreach($text as &$w){	
	if (preg_match('/^.+?\t[N](?!U).+?\t.+?\t.+?\s?$/', $w)){
		$w1 = preg_replace('/^(.+?\t[N].+?\t.+?\t)(.+?)$/','$2', $w);
		$morphology_N[] = trim($w1);
	}
}

if (empty($morphology_V)||empty($morphology_N)) {
	echo'DATA ERROR: No morphemes were recognised.';
	exit;
}
$text = array_filter($text);
//print_r($text);

//$morphology_V = array ("ound", "X", "X", "X", "is", "X", "X", "X", "X", "ed", "fell", "was", "was", "X", "ound", "ound", "was", "X", "ound", "made", "fell", "are", "X", "fell", "X", "ed", "came", "was", "fell", "was", "ound", "X", "was", "X", "X");




$dialog = '<div id="dialog" title="Information">
			<p>The file was successfully uploaded and the data were processed.</p>
			</div>';

//store text for download
$_SESSION['h'] = $text;	
		
include 'display_experiment.php';

?>