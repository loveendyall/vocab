<!DOCTYPE HTML>
<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>
			LancsLex: Lancaster vocab tool
		</title>
		<link rel="Index"         href="http://corpora.lancs.ac.uk/vocab/"     />
		<link  rel="stylesheet"   href="vocab1.css" type="text/css" media="all" />
		<link rel="shortcut icon" href="http://corpora.lancs.ac.uk/vocab/favicon.ico" />
		<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900,200italic,300italic,400italic,600italic,700italic,900italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
		<script src="clientside/show.js"></script>	
	
	  <!--Load the AJAX API-->
        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
        <script type="text/javascript">

          // Load the Visualization API and the piechart package.
          google.load('visualization', '1.0', {'packages':['corechart']});

          // Set a callback to run when the Google Visualization API is loaded.
          google.setOnLoadCallback(drawChart);

          // Callback that creates and populates a data table,
          // instantiates the pie chart, passes in the data and
          // draws it.
          function drawChart() {

            // Create the data table.
           var data = google.visualization.arrayToDataTable([
          ['Categiries', 'Number'],
          ['new-GSL words',     <?php echo $all_coverage;?>],
          ['other words',      <?php echo ($all_in_text - $all_coverage)?>]
        ]);
		
		

            
            // Set chart options
            var options = {
			  title: 'TEXT COVERAGE',
			  chartArea:{left:20,top:20,width:"80%",height:"80%"},	
			  pieHole: 0.4,
			  legend: {position: 'none'},
			  colors:['#333399','#B8008A'],
			  backgroundColor:'#F5F5F5'
			  
			};
            

            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
            chart.draw(data, options);
          }
        </script>
	
	
	</head>

<body>	
<div id="container">
		<?php header_page() ?>
	<div id="body">
		<!-- Body start -->
		<h1 id="heading">Results: text coverage</h1>
		<h2 id="subheading">English vocabulary interactive resource</h2>
		<hr/>
		<table border = "0" width = "100%">
			<tr>
				<td width = "50%">
				<p>Sentences analysed: <?php echo number_format($sentences)?> </p>
				<p>Words analysed: <?php echo number_format($all_in_text); ?> </p>
				<p>Words covered by the new-GSL: <?php echo number_format($all_coverage)." ($all_coverage_per_cent%)"; ?> </p>
				<br/>
				<br/>
				</td>
				<td width = "50%">
				<div id="chart_div"></div>
				</td>
			</tr>
		
		<table/>
		<hr>
		</br>
		<?php drawChartall($data2, $image);
		
		
		?>
		</br>
		<hr>
		</br>
		
		
		
		<table class = "CSSTableGenerator">
			<tr>
				<td> Word class </td>
				<td> Text total</td>
				<td>new-GSL 500</td>
				<td>new-GSL 1000</td>
				<td>new-GSL 2500</td>
				<td>Off list</td>
				<td>new-GSL TOTAL (%)</td>
				
			</tr>
			<tr>
				<td> NOUNS </td>
				<td title = "<?php echo display_pos_in_text($nouns_in_text)?>"><?php echo $nouns_in_text[0];?></td>
				<td title = "<?php echo display_pos_in_text($nouns_500)?>"> <?php echo $nouns_500[0];?></td>
				<td title = "<?php echo display_pos_in_text($nouns_1000)?>"> <?php echo $nouns_1000[0];?></td>
				<td title = "<?php echo display_pos_in_text($nouns_2500)?>"> <?php echo $nouns_2500[0];?></td>
				<td title = "<?php echo display_pos_in_text($nouns_offlist)?>"> <?php echo $nouns_offlist[0];?></td>
				<td> <?php echo "$nouns_all ($nouns_coverage_per_cent%)";?></td>
				</tr>
			<tr>
				<td> VERBS</td>
				<td title = "<?php echo display_pos_in_text($verbs_in_text)?>"><?php echo $verbs_in_text[0];?></td>
				<td title = "<?php echo display_pos_in_text($verbs_500)?>"> <?php echo $verbs_500[0];?></td>
				<td title = "<?php echo display_pos_in_text($verbs_1000)?>"> <?php echo $verbs_1000[0];?></td>
				<td title = "<?php echo display_pos_in_text($verbs_2500)?>"> <?php echo $verbs_2500[0];?></td>
				<td title = "<?php echo display_pos_in_text($verbs_offlist)?>"> <?php echo $verbs_offlist[0];?></td>
				<td> <?php echo "$verbs_all ($verbs_coverage_per_cent%)";?></td>
			</tr>
			<tr>
				<td> MODALS </td>
				<td title = "<?php echo display_pos_in_text($mod_in_text)?>"><?php echo $mod_in_text[0];?></td>
				<td title = "<?php echo display_pos_in_text($mod_500)?>"> <?php echo $mod_500[0];?></td>
				<td title = "<?php echo display_pos_in_text($mod_1000)?>"> <?php echo $mod_1000[0];?></td>
				<td title = "<?php echo display_pos_in_text($mod_2500)?>"> <?php echo $mod_2500[0];?></td>
				<td title = "<?php echo display_pos_in_text($mod_offlist)?>"> <?php echo $mod_offlist[0];?></td>
				<td> <?php echo "$mod_all ($mod_coverage_per_cent%)";?></td>
			</tr>
			<tr>
				<td> ADJECTIVES </td>
				<td title = "<?php echo display_pos_in_text($adj_in_text)?>"><?php echo $adj_in_text[0];?></td>
				<td title = "<?php echo display_pos_in_text($adj_500)?>"> <?php echo $adj_500[0];?></td>
				<td title = "<?php echo display_pos_in_text($adj_1000)?>"> <?php echo $adj_1000[0];?></td>
				<td title = "<?php echo display_pos_in_text($adj_2500)?>"> <?php echo $adj_2500[0];?></td>
				<td title = "<?php echo display_pos_in_text($adj_offlist)?>"> <?php echo $adj_offlist[0];?></td>
				<td> <?php echo "$adj_all ($adj_coverage_per_cent%)";?></td>
			</tr>
			<tr>
				<td> ADVERBS </td>
				<td title = "<?php echo display_pos_in_text($adv_in_text)?>"><?php echo $adv_in_text[0];?></td>
				<td title = "<?php echo display_pos_in_text($adv_500)?>"> <?php echo $adv_500[0];?></td>
				<td title = "<?php echo display_pos_in_text($adv_1000)?>"> <?php echo $adv_1000[0];?></td>
				<td title = "<?php echo display_pos_in_text($adv_2500)?>"> <?php echo $adv_2500[0];?></td>
				<td title = "<?php echo display_pos_in_text($adv_offlist)?>"> <?php echo $adv_offlist[0];?></td>
				<td> <?php echo "$adv_all ($adv_coverage_per_cent%)";?></td>
			</tr>
			<tr>
				<td> CONNECTORS (prepositions & conjunctions) </td>
				<td title = "<?php echo display_pos_in_text($con_in_text)?>"><?php echo $con_in_text[0];?></td>
				<td title = "<?php echo display_pos_in_text($con_500)?>"> <?php echo $con_500[0];?></td>
				<td title = "<?php echo display_pos_in_text($con_1000)?>"> <?php echo $con_1000[0];?></td>
				<td title = "<?php echo display_pos_in_text($con_2500)?>"> <?php echo $con_2500[0];?></td>
				<td title = "<?php echo display_pos_in_text($con_offlist)?>"> <?php echo $con_offlist[0];?></td>
				<td> <?php echo "$con_all ($con_coverage_per_cent%)";?></td>
			</tr>
			<tr>
				<td> PRONOUNS </td>
				<td title = "<?php echo display_pos_in_text($pron_in_text)?>"><?php echo $pron_in_text[0];?></td>
				<td title = "<?php echo display_pos_in_text($pron_500)?>"> <?php echo $pron_500[0];?></td>
				<td title = "<?php echo display_pos_in_text($pron_1000)?>"> <?php echo $pron_1000[0];?></td>
				<td title = "<?php echo display_pos_in_text($pron_2500)?>"> <?php echo $pron_2500[0];?></td>
				<td title = "<?php echo display_pos_in_text($pron_offlist)?>"> <?php echo $pron_offlist[0];?></td>
				<td> <?php echo "$pron_all ($pron_coverage_per_cent%)";?></td>
			</tr>
			<tr>
				<td> OTHER (gram. words) </td>
				<td title = "<?php echo display_pos_in_text($other_in_text)?>"><?php echo $other_in_text[0];?></td>
				<td title = "<?php echo display_pos_in_text($other_500)?>"> <?php echo $other_500[0];?></td>
				<td title = "<?php echo display_pos_in_text($other_1000)?>"> <?php echo $other_1000[0];?></td>
				<td title = "<?php echo display_pos_in_text($other_2500)?>"> <?php echo $other_2500[0];?></td>
				<td title = "<?php echo display_pos_in_text($other_offlist)?>"> <?php echo $other_offlist[0];?></td>
				<td> <?php echo "$other_all ($other_coverage_per_cent%)";?></td>
			</tr>
					
			
		</table>
	
		<br/>
		<hr/>
		<a href="display_text.php" target="blank"> Show text</a>
		<br/>
		<br/>
			<p><strong>How to cite?</strong></p>
			<p>Brezina, V. & Gablasova, D. (2015) "English vocabulary tool", available from http://corpora.lancs.ac.uk/vocab</p>
			<p>The analysis is based on: Brezina, V. & Gablasova, D. (2015). <a href="http://applij.oxfordjournals.org/content/36/1/1"> Is There a Core General Vocabulary? Introducing the New General Service List,<em></a> Applied Linguistics 36</em> (1), pp. 1-22, published online, August 2013.</p>
			
			<br/>
		<br/>
		
		<!-- Body end -->
	</div>
	<div id="footer">
		<!-- Footer start -->
		<p> <?php footer() ?></a> </p>
		<!-- Footer end -->
	</div>
</div>
</body>