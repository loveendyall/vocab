import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HttpClientModule } from '@angular/common/http';

import { NgbDropdownModule, NgbModalModule, NgbCollapseModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';

import { ParallaxScrollModule } from 'ng2-parallaxscroll';

import { ChartsModule } from 'ng2-charts';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { VocabListsComponent } from './vocab-lists/vocab-lists.component';
import { NewGslComponent } from './new-gsl/new-gsl.component';
import { LexisComponent } from './lexis/lexis.component';
import { MorphologyComponent } from './morphology/morphology.component';
import { SyntaxComponent } from './syntax/syntax.component';
import { SearchComponent } from './search/search.component';
import { AnalysisComponent } from './analysis/analysis.component';

@NgModule({
	declarations: [
		AppComponent,
		VocabListsComponent,
		NewGslComponent,
		LexisComponent,
		MorphologyComponent,
		SyntaxComponent,
		SearchComponent,
		AnalysisComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		BrowserAnimationsModule,
		FormsModule,
		HttpClientModule,
		NgbDropdownModule,
		NgbModalModule,
		NgbCollapseModule,
		NgbTooltipModule,
		ParallaxScrollModule,
		ChartsModule
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule { }
