import { Component, HostListener, ViewEncapsulation } from '@angular/core';

import { Router } from '@angular/router';

import {trigger, state, style, animate, transition, query, group} from '@angular/animations';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IParallaxScrollConfig} from 'ng2-parallaxscroll';

import { ApiService } from './api.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css'],
	animations: [
		trigger('toggleBanner', [
			state('false', style({
				'max-height': '0px',
				'opacity': 0.6,
				'padding': '0 2rem'
			})),
			state('true', style({
				'max-height': '70vh',
				'opacity': 1,
				'padding': '4rem 2rem 0 2rem'
			})),
			transition('false <=> true', [
				animate('300ms ease-in-out')
			])
		]),
		trigger('toggleNavbar', [
			state('void', style({
				'max-height': '0px'
			})),
			state('*', style({
				'max-height': '270px'
			})),
			transition('void <=> *', [
				animate('300ms ease-in-out')
			])
		]),
		trigger('toggleAuxNavbar', [
			state('void', style({
				'max-height': '0px'
			})),
			state('*', style({
				'max-height': '500px'
			})),
			transition('void <=> *', [
				animate('300ms ease-in-out')
			])
		]),
		trigger('routeAnimations', [
			transition('* => *', [
				group([
					query(
						':enter',
						[style({ position: 'absolute', left: '0px', top: '0px', opacity: 0 })],
						{ optional: true }
					),
					query(
						':leave',
						[style({ position: 'absolute', left: '0px', top: '0px', opacity: 1 })],
						{ optional: true }
					)
				]),
				group([
					query(
						':leave',
						[style({ opacity: 1 }), animate('50ms ease-in-out', style({ opacity: 0 }))],
						{ optional: true }
					),
					query(
						':enter',
						[style({ opacity: 0 }), animate('150ms ease-in-out', style({ opacity: 1 }))],
						{ optional: true }
					)
				])
			])
		])
	]
})
export class AppComponent {
	public navbarShow: boolean = true;
	public collapseBanner: boolean = true;
	
	public parallaxConfig: IParallaxScrollConfig = {
		speed: -0.09,
		axis: 'Y'
	};
	public searchTarget: any;
	public semCategory: {label: string, value: string} = {'label': 'Semantic Category', 'value': null};
	public wordClass: {label: string, value: string} = {'label': 'Word Class', 'value': null};
	
	public auxNavbarShow: boolean = true;
	
	constructor(private api: ApiService,
				public router: Router,
				public modalService: NgbModal) {
		router.events.subscribe(url => {
			console.log(url);
		});
	}
	
	@HostListener('window:resize', ['$event'])
	onResize(event) {
		if (event.target.innerWidth >= 678) {
			this.navbarShow = true;
		}
		if (event.target.innerWidth >= 576) {
			this.auxNavbarShow = true;
		}
	}
	
	changeSemCategory(value: string, label: string): void {
		this.semCategory = {
			'label': label,
			'value': value
		};
	}
	
	changeWordClass(value: string, label: string): void {
		this.wordClass = {
			'label': label,
			'value': value
		};
	}
	
	searchByTarget(): void {
		if (this.searchTarget != '') {
			this.router.navigate(['search'], {queryParams: {q: this.searchTarget}});
		}
	}
	
	searchByCategory(): void {
		this.router.navigate(['search'], {queryParams: {c: 'yes', u: this.semCategory.value, p: this.wordClass.value}});
		this.modalService.dismissAll();
	}
}
