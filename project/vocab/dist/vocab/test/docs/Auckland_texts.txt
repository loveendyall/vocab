***Task 1***

Russian artist Svetlana Petrova has become known for her online artwork of famous portraits featuring her big ginger cat Zarathustra.

Ahead of a new exhibition bringing the internet meme into a physical setting, the artist tells the BBC why she first created the artwork and how digital technology is helping to create new art forms.

"I lost my mother in 2008 and she left me Zarathustra. I got horrible depression after her death and for two years I was unable to do something creative. By chance a friend asked me 'why don't you make an art project with your cat because he's so funny'



***Task 3***

The first Maori settlements were mostly located around harbours or river mouths where fish and sea birds lived abundantly. New Zealand, unlike their original islands, was abundant in wild game, so the Maori used both agriculture and hunting to sustain the Iwi. One of their biggest sources of food was the moa, a large flightless bird. The moas varied in size from the height of a turkey, to 3.7 metres high. Unfortunately, this made them easy targets, and they became extinct due to over-hunting by about 1500. As a result of this, the Maori switched back to agriculture.

Gradually, the Maori dispersed themselves over New Zealand in different tribes, with different chiefs as leaders. The different tribes became more aggressive however, and inter-tribal warfare became much more frequent over time. This led to the advent of the pa (a fortified village). An average pa included ditches, banks and palisades as protection. New Zealand eventually became divided up by tribal territories which were recognised by other tribes with predominant land features (rivers, mountains, lakes). This culture remained up until the 18th Century, when the Europeans came to New Zealand.

***Task 4***

Gradually, the Maori dispersed themselves over New Zealand in different tribes, with different chiefs as leaders. The different tribes became more aggressive however, and inter-tribal warfare became much more frequent over time. This led to the advent of the pa (a fortified village). An average pa included ditches, banks and palisades as protection.

After some time, the Maori moved to different parts of New Zealand and formed different tribes, with different chiefs as leaders. The different tribes became more aggressive however, and inter-tribal wars became much more frequent over time. This led to the introduction of the pa (a fortified village). An average pa included ditches, banks and palisades as protection.

The first Maori settlements were mostly located around harbours or river mouths where fish and sea birds lived abundantly. New Zealand, unlike their original islands, was abundant in wild game, so the Maori used both agriculture and hunting to sustain the Iwi. One of their biggest sources of food was the moa, a large flightless bird. The moas varied in size from the height of a turkey, to 3.7 metres high. Unfortunately, this made them easy targets, and they became extinct due to over-hunting by about 1500. As a result of this, the Maori switched back to agriculture.

