import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VocabListsComponent } from './vocab-lists.component';

describe('VocabListsComponent', () => {
  let component: VocabListsComponent;
  let fixture: ComponentFixture<VocabListsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VocabListsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VocabListsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
