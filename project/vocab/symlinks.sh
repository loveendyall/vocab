#!/bin/sh


cp ../../../php_backend/* ./
mkdir ./docs
cp ../../../docs/* ./docs/
ln -s ./index.html ./syntax
ln -s ./index.html ./new-gsl
ln -s ./index.html ./lexis
ln -s ./index.html ./morphology
ln -s ./index.html ./vocab-lists
ln -s ./index.html ./search
ln -s ./index.html ./analysis


ln -s ./dist/vocab/index.html ./dist/vocab/syntax
ln -s ./dist/vocab/index.html ./dist/vocab/new-gsl
ln -s ./dist/vocab/index.html ./dist/vocab/lexis
ln -s ./dist/vocab/index.html ./dist/vocab/morphology