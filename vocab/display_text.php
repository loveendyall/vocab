<?php session_start(); ?>
<!DOCTYPE HTML>
<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>
			LancsLex: Lancaster vocab tool
		</title>
		<link rel="Index"         href="http://corpora.lancs.ac.uk/vocab/"     />
		<link  rel="stylesheet"   href="vocab1.css" type="text/css" media="all" />
		<link rel="shortcut icon" href="http://corpora.lancs.ac.uk/vocab/favicon.ico" />
	<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900,200italic,300italic,400italic,600italic,700italic,900italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	
	
	</head>
<?php include 'CODE/functions.php'; ?>
<body>	
<div id="container">
		<?php header_page() ?>
	<div id="body">
	

			<!-- Body start -->
			<h1 id="heading">Results: text display</h1>
			<h2 id="subheading">English vocabulary interactive resource</h2>
			<hr/>
			
			<p><?php colour_display($_SESSION['e']);?></p>
			<hr/>
			
			
			<br/>
			<!-- Body end -->
		
				
		
		
	</div>
	<div id="footer">
		<!-- Footer start -->
		<p> <?php footer() ?></a> </p>
		<!-- Footer end -->
	</div>
</div>
</body>