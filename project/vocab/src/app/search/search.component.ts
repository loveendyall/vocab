import { Component, OnInit } from '@angular/core';
import {ApiService} from '../api.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
	public errorMsg: string;
	public searchTargetRes: any[];
	public categorySearchRes: any;
	
	constructor(private api: ApiService,
				private route: ActivatedRoute,
				private router: Router) { }
	
	ngOnInit() {
		this.route.queryParams.subscribe((params: any) => {
			this.api.search(params.q, params.c, params.u, params.p).subscribe((res: any) => {
				this.errorMsg = undefined;
				this.searchTargetRes = [];
				this.categorySearchRes = undefined;
				if (res['Semantic category'] != undefined) {
					this.categorySearchRes = {};
					this.categorySearchRes['semCategory'] = res['Semantic category'];
					this.categorySearchRes['wordClass'] = res['Word class'];
					if (res.results.type != 'found') {
						this.errorMsg = res.results.type;
					} else {
						this.categorySearchRes['results'] = res.results['res'];
					}
				} else if (res.res != undefined) {
					if (res.res.length > 0) {
						this.searchTargetRes = res.res;
					} else {
						this.errorMsg = res.type;
					}
				} else {
					this.errorMsg = res.type;
				}
			});
		});
	}
}
