<?php
function display_categories($pos, $usas){
	$categories = array();
	if($pos=="N")
		$categories[0]="noun";
	elseif($pos=="V")
		$categories[0]="verb";
	elseif($pos=="M")
		$categories[0]="modal";
	elseif($pos=="J")
		$categories[0]="adjective";
	elseif($pos=="R")
		$categories[0]="adverb";
	elseif($pos=="C")
		$categories[0]="connector";
	elseif($pos=="P")
		$categories[0]="pronoun";
	elseif($pos=="X")
		$categories[0]="other";
	else
		$categories[0]="undefined";


	if($usas=="A")
		$categories[1]="general terms";
	elseif($usas=="B")
		$categories[1]="body";
	elseif($usas=="C")
		$categories[1]="arts and crafts";
	elseif($usas=="E")
		$categories[1]="emotion";
	elseif($usas=="F")
		$categories[1]="food and farming";
	elseif($usas=="G")
		$categories[1]="government and public";
	elseif($usas=="H")
		$categories[1]="housing and architecture";
	elseif($usas=="I")
		$categories[1]="money";
	elseif($usas=="K")
		$categories[1]="entertainment and sports";
	elseif($usas=="L")
		$categories[1]="life and living things";
	elseif($usas=="M")
		$categories[1]="movement,location and transport";
	elseif($usas=="N")
		$categories[1]="numbers and measurement";
	elseif($usas=="O")
		$categories[1]="substances and objects";
	elseif($usas=="P")
		$categories[1]="education";
	elseif($usas=="Q")
		$categories[1]="language and communication";
	elseif($usas=="S")
		$categories[1]="states and processes";
	elseif($usas=="T")
		$categories[1]="time";
	elseif($usas=="W")
		$categories[1]="world and environment";
	elseif($usas=="X")
		$categories[1]="psychological actions and processes";
	elseif($usas=="Y")
		$categories[1]="science and technology";
	elseif($usas=="Z")
		$categories[1]="grammar";
	else
		$categories[1]="undefined";
	return $categories;
}

function search_for_categories($pos, $usas, $file){

	if($usas != '' || $pos != '') {

		// the following line prevents the browser from parsing this as HTML.
		//header('Content-Type: text/plain');

		// get the file contents, assuming the file to be readable (and exist)
		$contents = file_get_contents($file);
		// escape special characters in the query
		//$pattern = preg_quote($usas, '/');
		// finalise the regular expression, matching the whole line
		if($usas != ''&& $pos==''){
			$usas = $usas."[0-9]+";
			$pattern = "/^.*\t.*\t$usas.*\$/ m";

		}
		elseif($pos != '' && $usas==''){
			$pos = $pos."\t";
			$pattern = "/^.*$pos.*\$/ m";
		}
		elseif($pos != '' && $usas!=''){
			$pos = $pos."\t";
			$pattern = "/^.*$pos$usas.*\$/ m";
		}
		// search, and store all matching occurences in $matches
		if(preg_match_all($pattern, $contents, $matches)){
			foreach ($matches[0] as &$value){
				$value = preg_replace('/^(\d+\t)([a-zA-Z- ]+\t)N.*/', '$1 $2 (noun)', $value);
				$value = preg_replace('/^(\d+\t)([a-zA-Z- ]+\t)V.*/', '$1 $2 (verb)', $value);
				$value = preg_replace('/^(\d+\t)([a-zA-Z- ]+\t)M.*/', '$1 $2 (mod)', $value);
				$value = preg_replace('/^(\d+\t)([a-zA-Z- ]+\t)J.*/', '$1 $2 (adj)', $value);
				$value = preg_replace('/^(\d+\t)([a-zA-Z- ]+\t)R.*/', '$1 $2 (adv)', $value);
				$value = preg_replace('/^(\d+\t)([a-zA-Z- ]+\t)P.*/', '$1 $2 (pron)', $value);
				$value = preg_replace('/^(\d+\t)([a-zA-Z- ]+\t)C.*/', '$1 $2 (con)', $value);
				$value = preg_replace('/^(\d+\t)([a-zA-Z- ]+\t)X.*/', '$1 $2 (gram)', $value);
				$value = preg_split('/[\s]+/', $value);
			}
			return array("type" => "found", "res" => $matches[0]);
		}
		else{
			return array("type" => "No matches were found.");
		}
	}
	else return array("type" => "No search categories were entered.");
}


function search ($searchfor, $file){

	if (!empty($searchfor))
	{

		// the following line prevents the browser from parsing this as HTML.
		//header('Content-Type: text/plain');

		// get the file contents, assuming the file to be readable (and exist)
		$contents = file_get_contents($file);
		// escape special characters in the query
		$pattern = preg_quote($searchfor, '/');
		// finalise the regular expression, matching the whole line
		$pattern = "/^$pattern.*\$/i m";
		// search, and store all matching occurences in $matches
		if(preg_match_all($pattern, $contents, $matches)){
			return array("type" => "found", "res" => $matches[0]);
		}
		else
		{
			return array("type" => "No matches were found.");
		}
	} else return array("type" => "No search was term entered.");
}


//analyse functions start here

function count_POS_in_text_and_get_items($text_array, $pos){

	// create an subset of the text array based on the $pos (part_of_speech)
	$text_array_subset = array();
	foreach ($text_array as $key => $value) {
		if (preg_match("/^".$pos."_/",$key)){
			$text_array_subset[$key] = $value;
		}
	}


	//loop through array and count items
	$count = 0;
	$items = array();;
	foreach ($text_array_subset as $key => $value){ //do something for each value in an array
		$count += $value;
		$items[] = $key.' ('.$value.')' ;
	}
	asort($items);
	return array($count, $items);
}


function get_word_class_counts_and_items($GSL_array, $text_array, $pos, $band){

	// create an subset of the text array based on the $pos (part_of_speech)
	$text_array_subset = array();
	foreach ($text_array as $key => $value) {
		if (preg_match("/^".$pos."_/",$key)){
			$text_array_subset[$key] = $value;
		}
	}

	//create a combined array including all important info
	$intersect = array_intersect_key($text_array_subset, $GSL_array);
	$intersect1 = array_intersect_key($GSL_array, $text_array_subset);
	$final_intersect = array_merge_recursive($intersect1, $intersect);

	//loop through array and count items
	$count = 0;
	$items = array();;

	foreach ($final_intersect as $value){ //do something for each value in an array
		if ($value[2] ==$band){ //define band
			$count += $value[4];
			$items[] = $value[0].' ('.$value[4].')';
		}
	}
	asort($items);
	return array($count, $items);
}

function get_offlist_counts_and_items($GSL_array, $text_array, $pos){

	// create an subset of the text array based on the $pos (part_of_speech)
	$text_array_subset = array();
	foreach ($text_array as $key => $value) {
		if (preg_match("/^".$pos."_/",$key)){
			$text_array_subset[$key] = $value;
		}
	}

	//create a combined array including all important info
	$intersect = array_diff_key($text_array_subset, $GSL_array);


	//loop through array and count items
	$count = 0;
	$items = array();;

	foreach ($intersect as $key => $value){ //do something for each value in an array


		$count += $value;
		$items[] = $key.' ('.$value.')' ;

	}
	asort($items);
	return array($count, $items);
}

function display_words() {
	return "here are some words";

}



function process($f, $proper, $number) {
	$f= str_replace("</s>","",$f);
	$f= str_replace("<s>","",$f);
//	$f= preg_replace("/[\r\n]/","",$f);
	//$f= str_replace("'","",$f);
	//$f= str_replace("’","",$f);
	//$f= preg_replace('/,\t,\t,/','',$f);


	//$f= preg_replace('/^([A-Za-z0-9-\']*)?\t/','',$f);

	$f= preg_replace('/\t/','_',$f);

	if ($proper==1){
		$f= preg_replace('/(^.*_)NP_God/','$1N_God',$f);// to exclude from filtering
		$f= preg_replace('/(^.*_)NP_Internet/','$1N_Internet',$f);
		$f= preg_replace('/(^.*_)NP_South/','$1N_South',$f);
		$f= preg_replace('/(^.*_)NP_West/','$1N_West',$f);
		$f= preg_replace('/(^.*_)NP_North/','$1N_North',$f);
		$f= preg_replace('/(^.*_)NP_East/','$1N_East',$f);
		$f= preg_replace('/(^.*_)NP_East/','$1N_East',$f);
		$f= preg_replace('/(^.*_)NP_Mother/','$1N_mother',$f);
		$f= preg_replace('/(^.*_)NP_Father/','$1N_father',$f);
		$f= preg_replace('/(^.*_)NP_Aunt/','$1N_aunt',$f);
		$f= preg_replace('/(^.*_)NP_Uncle/','$1N_uncle',$f);
		$f= preg_replace('/(^.*_)NP_Monday/','$1N_Monday',$f);
		$f= preg_replace('/(^.*_)NP_Tuesday/','$1N_Tuesday',$f);
		$f= preg_replace('/(^.*_)NP_Wednesday/','$1N_Wednesday',$f);
		$f= preg_replace('/(^.*_)NP_Thursday/','$1N_Thursday',$f);
		$f= preg_replace('/(^.*_)NP_Friday/','$1N_Friday',$f);
		$f= preg_replace('/(^.*_)NP_Saturday/','$1N_Saturday',$f);
		$f= preg_replace('/(^.*_)NP_Sunday/','$1N_Sunday',$f);
		$f= preg_replace('/(^.*_)NP_January/','$1N_January',$f);
		$f= preg_replace('/(^.*_)NP_February/','$1N_February',$f);
		$f= preg_replace('/(^.*_)NP_March/','$1N_March',$f);
		$f= preg_replace('/(^.*_)NP_April/','$1N_April',$f);
		$f= preg_replace('/(^.*_)NP_May/','$1N_May',$f);
		$f= preg_replace('/(^.*_)NP_June/','$1N_June',$f);
		$f= preg_replace('/(^.*_)NP_July/','$1N_July',$f);
		$f= preg_replace('/(^.*_)NP_August/','$1N_August',$f);
		$f= preg_replace('/(^.*_)NP_September/','$1N_September',$f);
		$f= preg_replace('/(^.*_)NP_October/','$1N_October',$f);
		$f= preg_replace('/(^.*_)NP_November/','$1N_November',$f);
		$f= preg_replace('/(^.*_)NP_December/','$1N_December',$f);

		$f= preg_replace('/(^.*_)NP_Christmas/','$1N_Christmas',$f);
		$f= preg_replace('/(^.*_)NP_Xmas/','$1N_Xmas',$f);
		$f= preg_replace('/(^.*_)NP_TV/','$1N_TV',$f);
		$f= preg_replace('/(^.*_)NP_CD/','$1N_CD',$f);

		$f= preg_replace('/(^.*_)NPS?_/','$1Y_',$f);//proper nouns
		$f= preg_replace('/(^(?!T-shirt)[A-Z].*_)N.*_/','$1Y_',$f);//proper nouns mis-tagged as nouns
		//$f= preg_replace('/(^.*_)NPS?_.*/','$1',$f);//delete proper nouns with the exception of those above

	}

	if ($number==1){
		$f= preg_replace('/(^.*_)[A-Z]+_@card@/','$1Y_@card@',$f);
		$f= preg_replace('/(^.*_)CD_0/','$1Y_0',$f);
		$f= preg_replace('/(^.*_)CD_1/','$1Y_1',$f);
		$f= preg_replace('/(^.*_)CD_2/','$1Y_2',$f);
		$f= preg_replace('/(^.*_)CD_3/','$1Y_3',$f);
		$f= preg_replace('/(^.*_)CD_4/','$1Y_4',$f);
		$f= preg_replace('/(^.*_)CD_5/','$1Y_5',$f);
		$f= preg_replace('/(^.*_)CD_6/','$1Y_6',$f);
		$f= preg_replace('/(^.*_)CD_7/','$1Y_7',$f);
		$f= preg_replace('/(^.*_)CD_8/','$1Y_8',$f);
		$f= preg_replace('/(^.*_)CD_9/','$1Y_9',$f);
		$f= preg_replace('/(^[0-9].*_)[A-Z].*_/','$1Y_',$f);//numbers mis-tagged as something else
	}

	$f= preg_replace('/(^.*_)VBG_accord/','$1C_according',$f);//according to

	$f= preg_replace('/(^.*_)SYM_/','$1S_',$f);//symbols
	$f= preg_replace('/(^.*_)N[A-Z]*_/','$1N_',$f);//nouns
	$f= preg_replace('/(^.*_)JJ[A-Z]?_/','$1J_',$f);//adjectives
	$f= preg_replace('/(^.*_)V[A-Z]*_/','$1V_',$f);//verbs
	$f= preg_replace('/(^.*_)MD_/','$1M_',$f);//modals
	$f= preg_replace('/(^.*_)PP\$?_/','$1P_',$f);//pronouns
	$f= preg_replace('/(^.*_)WP\$?_/','$1P_',$f);//pronouns
	$f= preg_replace('/(^.*_)WDT_/','$1P_',$f);//pronouns
	$f= preg_replace('/(^.*_)IN_/','$1C_',$f);//prep/conjunctions
	$f= preg_replace('/(^.*_)CC_/','$1C_',$f);//prep/conjunctions
	$f= preg_replace('/(^.*_)CD_/','$1X_',$f);//numerals
	$f= preg_replace('/(^.*_)DT_/','$1X_',$f);//determiners
	$f= preg_replace('/(^.*_)EX_/','$1X_',$f);//existential there
	$f= preg_replace('/(^.*_)PDT_/','$1X_',$f);//predeterminer
	$f= preg_replace('/(^.*_)RP_/','$1X_',$f);//particle
	$f= preg_replace('/(^.*_)RB[A-Z]*_/','$1R_',$f);//adverb
	$f= preg_replace('/(^.*_)WRB_/','$1R_',$f);//adverb
	$f= preg_replace('/(^.*_)TO_/','$1X_',$f);//to particle
	$f= preg_replace('/(^.*_)UH_/','$1X_',$f);//interjections

	//lower case filter applied to: verbs, modals, adverbs, pronouns, other grammatical words
	$f= preg_replace_callback("/(^.*_V_)([A-Za-z0-9-\']*?\s?$)/", function($m) {return($m[1].strtolower($m[2]));},$f);
	$f= preg_replace_callback("/(^.*_M_)([A-Za-z0-9-\']*?\s?$)/",function($m) {return($m[1].strtolower($m[2]));},$f);
	$f= preg_replace_callback("/(^.*_R_)([A-Za-z0-9-\']*?\s?$)/",function($m) {return($m[1].strtolower($m[2]));},$f);
	$f= preg_replace_callback("/(^.*_P_)(?!I)([A-Za-z0-9-\']*?\s?$)/",function($m) {return($m[1].strtolower($m[2]));},$f);
	$f= preg_replace_callback("/(^.*_X_)([A-Za-z0-9-\']*?\s?$)/",function($m) {return($m[1].strtolower($m[2]));},$f);



	$f= preg_replace('/(^.*_)R_yes(\s?$)/','$1X_yes$2',$f);//interjections
	$f= preg_replace('/(^.*_)R_no(\s?$)/','$1X_no$2',$f);//interjections

	$f= preg_replace('/(^.*_)X_an(\s?$)/','$1X_a$2',$f);//article

	$f= preg_replace('/(^.*_)R_n\'t(\s?$)/','$1R_not$2',$f);//not - lumping
	$f= preg_replace('/(^.*_)R_nt(\s?$)/','$1R_not$2',$f);//not - lumping
	$f= preg_replace('/(^.*_)X_not(\s?$)/','$1R_not$2',$f);//not - lumping

	$f= preg_replace('/(^.*_)N_lot(\s?$)/','$1X_lot$2',$f);//lot of -correction

	$f= preg_replace('/(^.*_)P_me(\s?$)/','$1P_I$2',$f);//pron - lumping
	$f= preg_replace('/(^.*_)P_him(\s?$)/','$1P_he$2',$f);//pron - lumping
	$f= preg_replace('/(^.*_)P_her(\s?$)/','$1P_she$2',$f);//pron - lumping
	$f= preg_replace('/(^.*_)P_us(\s?$)/','$1P_we$2',$f);//pron - lumping
	$f= preg_replace('/(^.*_)P_them(\s?$)/','$1P_they$2',$f);//pron - lumping


	$f= preg_replace('/(^.*_)JJ_mine(\s?$)/','$1P_my$2',$f);//pron-correction
	$f= preg_replace('/(^.*_)P_yours(\s?$)/','$1P_your$2',$f);//pron - lumping
	$f= preg_replace('/(^.*_)P_hers(\s?$)/','$1P_her$2',$f);//pron - lumping
	$f= preg_replace('/(^.*_)P_ours(\s?$)/','$1P_our$2',$f);//pron - lumping
	$f= preg_replace('/(^.*_)P_theirs(\s?$)/','$1P_their$2',$f);//pron - lumping

	$f= preg_replace('/(^.*_)[A-Z]+_[sS]omebody(\s?$)/','$1P_somebody$2',$f);//pron - lumping
	$f= preg_replace('/(^.*_)[A-Z]+_[sS]omeone(\s?$)/','$1P_someone$2',$f);//pron - lumping
	$f= preg_replace('/(^.*_)[A-Z]+_[sS]omething(\s?$)/','$1P_something$2',$f);//pron - lumping

	$f= preg_replace('/(^.*_)[A-Z]+_[aA]nybody(\s?$)/','$1P_anybody$2',$f);//pron - lumping
	$f= preg_replace('/(^.*_)[A-Z]+_[aA]nyone(\s?$)/','$1P_anyone$2',$f);//pron - lumping
	$f= preg_replace('/(^.*_)[A-Z]+_[aA]nything(\s?$)/','$1P_anything$2',$f);//pron - lumping

	$f= preg_replace('/(^.*_)[A-Z]+_[nN]obody(\s?$)/','$1P_nobody$2',$f);//pron - lumping
	$f= preg_replace('/(^.*_)[A-Z]+_[nN]othing(\s?$)/','$1P_nothing$2',$f);//pron - lumping

	$f= preg_replace('/(^.*_)[A-Z]+_[eE]verybody(\s?$)/','$1P_everybody$2',$f);//pron - lumping
	$f= preg_replace('/(^.*_)[A-Z]+_[eE]verything(\s?$)/','$1P_everything$2',$f);//pron - lumping
	$f= preg_replace('/(^.*_)[A-Z]+_[eE]veryone(\s?$)/','$1P_everyone$2',$f);//pron - lumping

	$f= preg_replace('/(^.*_)N_Mr(\s?$)/','$1X_Mr$2',$f);//abbreviations - lumping
	$f= preg_replace('/(^.*_)N_Mr.(\s?$)/','$1X_Mr$2',$f);//abbreviations - lumping
	$f= preg_replace('/(^.*_)N_Mrs(\s?$)/','$1X_Mrs$2',$f);//abbreviations - lumping
	$f= preg_replace('/(^.*_)N_Mrs.(\s?$)/','$1X_Mrs$2',$f);//abbreviations - lumping
	$f= preg_replace('/(^.*_)N_Dr(\s?$)/','$1X_Dr$2',$f);//abbreviations - lumping
	$f= preg_replace('/(^.*_)N_Dr.(\s?$)/','$1X_Dr$2',$f);//abbreviations - lumping
	$f= preg_replace('/(^.*_)N_Ms(\s?$)/','$1X_Ms$2',$f);//abbreviations - lumping
	$f= preg_replace('/(^.*_)N_Ms.(\s?$)/','$1X_Ms$2',$f);//abbreviations - lumping

	$f= preg_replace('/(^.*_)J_first(\s?$)/','$1X_first$2',$f);//numbers - lumping
	$f= preg_replace('/(^.*_)J_second(\s?$)/','$1X_second$2$2',$f);//numbers - lumping
	$f= preg_replace('/(^.*_)J_third(\s?$)/','$1X_third$2',$f);//numbers - lumping
	$f= preg_replace('/(^.*_)N_eleven(\s?$)/','$1X_eleven$2',$f);//numbers - lumping
	$f= preg_replace('/(^.*_)N_twelve(\s?$)/','$1X_twelve$2',$f);//numbers - lumping

	$f= preg_replace('/(^.*_)R_south(\s?$)/','$1N_South$2',$f);//directions
	$f= preg_replace('/(^.*_)[A-Z]_north(\s?$)/','$1N_North$2',$f);//directions
	$f= preg_replace('/(^.*_)[A-Z]_west(\s?$)/','$1N_West$2',$f);//directions
	$f= preg_replace('/(^.*_)[A-Z]_east(\s?$)/','$1N_East$2',$f);//directions

	$f= preg_replace('/(^.*_)J_much(\s?$)/','$1X_much$2',$f);//much
	$f= preg_replace('/(^.*_)J_many(\s?$)/','$1X_many$2',$f);//many
	$f= preg_replace('/(^.*_)J_most(\s?$)/','$1X_most$2',$f);//most
	$f= preg_replace('/(^.*_)J_more(\s?$)/','$1X_more$2',$f);//more
	$f= preg_replace('/(^.*_)J_few(\s?$)/','$1X_few$2',$f);//few
	$f= preg_replace('/(^.*_)N_plenty(\s?$)/','$1X_plenty$2',$f);//plenty
	$f= preg_replace('/(^.*_)[A-Z]_that(\s?$)/','$1X_that$2',$f);//that
	$f= preg_replace('/(^.*_)[A-Z]_now(\s?$)/','$1R_now$2',$f);//now
	$f= preg_replace('/(^.*_)[A-Z]_such(\s?$)/','$1X_such$2',$f);//such
	$f= preg_replace('/(^.*_)[A-Z]_One/','$1X_one$2',$f);//one


	//lemmatization and tagging errors
	$f= preg_replace('/(^.*_)N_stays(\s?$)/','$1N_stay$2',$f);//stays
	$f= preg_replace('/(^.*_)M_ca(\s?$)/','$1M_can$2',$f);//ca
	$f= preg_replace('/(^.*_)M_wo(\s?$)/','$1M_will$2',$f);//wo as part of won't
	$f= preg_replace('/(^.*_)M_sha(\s?$)/','$1M_shall$2',$f);//sha as part of shan't
	$f= preg_replace('/(^.*_)M_need(\s?$)/','$1V_need$2',$f);//need to is not classified as modal
	$f= preg_replace('/(^.*_)X_please(\s?$)/','$1R_please$2',$f);//please
	$f= preg_replace('/(^.*_)C_yet(\s?$)/','$1R_yet$2',$f);//yet
	$f= preg_replace('/(^.*_)N_%(\s?$)/','$1Y_%$2',$f);//%


	return $f;

}


// coverage function to avoid division by zero
function calculate_coverage ($wordclass_all, $wordclass_in_text){
	if($wordclass_in_text != 0)
		$wordclass_coverage_per_cent = round(($wordclass_all/$wordclass_in_text * 100),2);
	else
		$wordclass_coverage_per_cent = 0;
	return $wordclass_coverage_per_cent;
}




//analysis display functions
function display_pos_in_text($array) {
	$new_array = $array[1];
	foreach ($new_array as $key => $value) {
		$new_array[$key] = preg_replace("/^[A-Z]_/", "", $value);
	}
	$words_string = implode(", ",$new_array);
	return $words_string;
}

//analysis display functions
//function display_individual_items($array) {
//
//	foreach ($array as $key => $value) {
//		$new_array[$key] = preg_replace("/^[A-Z]_/", "", $value);
//	}
//
//	sort($new_array);
//	$words_string = implode(", ",$new_array);
//	return $words_string;
//}

function find_gsl_status($string) {
	global $new_GSL_nouns, $new_GSL_verbs, $new_GSL_mod, $new_GSL_adj, $new_GSL_adv, $new_GSL_con, $new_GSL_pron, $new_GSL_other;
	$output="";
	if(preg_match('/N_.*/',$string)){
		if (array_key_exists("$string",$new_GSL_nouns))
			$output ="_".$new_GSL_nouns[$string][2]."_".$new_GSL_nouns[$string][1]."_".$new_GSL_nouns[$string][3]."_N";
		else $output ="_N";
	}
	if(preg_match('/V_.*/',$string)){
		if (array_key_exists("$string",$new_GSL_verbs))
			$output ="_".$new_GSL_verbs[$string][2]."_".$new_GSL_verbs[$string][1]."_".$new_GSL_verbs[$string][3]."_V";
		else $output ="_V";
	}
	if(preg_match('/M_.*/',$string)){
		if (array_key_exists("$string",$new_GSL_mod))
			$output ="_".$new_GSL_mod[$string][2]."_".$new_GSL_mod[$string][1]."_".$new_GSL_mod[$string][3]."_M";
		else $output ="_M";
	}
	if(preg_match('/J_.*/',$string)){
		if (array_key_exists("$string",$new_GSL_adj))
			$output ="_".$new_GSL_adj[$string][2]."_".$new_GSL_adj[$string][1]."_".$new_GSL_adj[$string][3]."_J";
		else $output ="_J";
	}
	if(preg_match('/R_.*/',$string)){
		if (array_key_exists("$string",$new_GSL_adv))
			$output ="_".$new_GSL_adv[$string][2]."_".$new_GSL_adv[$string][1]."_".$new_GSL_adv[$string][3]."_R";
		else $output ="_R";
	}
	if(preg_match('/C_.*/',$string)){
		if (array_key_exists("$string",$new_GSL_con))
			$output ="_".$new_GSL_con[$string][2]."_".$new_GSL_con[$string][1]."_".$new_GSL_con[$string][3]."_C";
		else $output ="_C";
	}
	if(preg_match('/P_.*/',$string)){
		if (array_key_exists("$string",$new_GSL_pron))
			$output ="_".$new_GSL_pron[$string][2]."_".$new_GSL_pron[$string][1]."_".$new_GSL_pron[$string][3]."_P";
		else $output ="_P";
	}
	if(preg_match('/X_.*/',$string)){
		if (array_key_exists("$string",$new_GSL_other))
			$output ="_".$new_GSL_other[$string][2]."_".$new_GSL_other[$string][1]."_".$new_GSL_other[$string][3]."_X";
		else $output ="_X";
	}
	return $output;
}

function colour_display($array){

	$output = [];

	foreach($array as $value){

		if(preg_match('/^(?!(n\'t|\'re|\'m|\'s))\w.*/',$value))
			$space=" ";
		elseif(preg_match('/^\(.*/',$value))
			$space=" ";
		elseif(preg_match('/^\[.*/',$value))
			$space=" ";
		elseif(preg_match('/^\{.*/',$value))
			$space=" ";
		elseif(preg_match('/^\".*/',$value))
			$space=" ";
		elseif(preg_match('/^\).*/',$value))
			$space=" ";
		elseif(preg_match('/^\].*/',$value))
			$space=" ";
		elseif(preg_match('/^\}.*/',$value))
			$space=" ";
		else
			$space ="";

		$word = preg_replace('/^(.*)_.*_.*_.*_.*\s?$/','$1',$value);
		$word1 = preg_replace('/^(.*)_[NVJRCPX]\s?$/','$1',$value);
		$rank = preg_replace('/^.*_.*_(.*)_.*_.*\s?$/','$1',$value);

		if (preg_match('/^.*_.*_.*_1_.*\s?$/',$value))
			$source ="lexical innovation";
		elseif(preg_match('/^.*_.*_.*_2_.*\s?$/',$value))
			$source ="US supplement";
		else
			$source ="core";

		if (preg_match('/^.*_1_.*_2_.*\s?$/',$value))
			$output[] = array("style" => 1, "color" => 0, "title" => "rank: ".$rank.", ".$source, "value" => $space.$word);
		elseif (preg_match('/^.*_[2,3]_.*_2_.*\s?$/',$value))
			$output[] = array("style" => 0, "color" => 0, "title" => "rank: ".$rank.", ".$source, "value" => $space.$word);
		else{
			if(preg_match('/^.*_1_.*_.*_.*\s?$/',$value)){
				$output[] = array("style" => 1, "color" => 1, "title" => "rank: ".$rank.", ".$source, "value" => $space.$word);
			}
			elseif(preg_match('/^.*_2_.*_.*_.*\s?$/',$value)){
				$output[] = array("style" => 0, "color" => 1, "title" => "rank: ".$rank.", ".$source, "value" => $space.$word);
			}
			elseif(preg_match('/^.*_3_.*_.*_.*\s?$/',$value)){
				$output[] = array("style" => 0, "color" => 0, "title" => "rank: ".$rank.", ".$source, "value" => $space.$word);
			}
			else
				$output[] = array("style" => 0, "color" => 2, "title" => "", "value" => $space.$word1);
		}
	}
	return $output;
}
?>