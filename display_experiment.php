<!DOCTYPE HTML>
<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>
			LancsLex: Lancaster vocab tool
		</title>
		<link rel="Index"         href="http://corpora.lancs.ac.uk/vocab/"     />
		<link  rel="stylesheet"   href="vocab1.css" type="text/css" media="all" />
		<link rel="shortcut icon" href="http://corpora.lancs.ac.uk/vocab/favicon.ico" />
		<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900,200italic,300italic,400italic,600italic,700italic,900italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
		<script src="clientside/show.js"></script>	
		<script src="clientside/ajaxsbmt.js" type="text/javascript"></script>
		<script src="clientside/jquery-1.10.2.js"></script>
		
		<script src="clientside/jquery-ui.js"></script>
		<link  rel="stylesheet"   href="style/jquery-ui.css" type="text/css" media="all" />
		<script>
			$(function() {
			$( "#dialog" ).dialog();
			});
		</script>
	
	</head>

<body>	
<div id="container">
		<?php header_page() ?>
	<div id="body">
	

			<!-- Body start -->
			<?php echo $dialog ?>
			<h1 id="heading">Results: complexity measures</h1>
			<h2 id="subheading">Alpha version</h2>
			<hr/>
			<p> <font color = "blue"> SETTINGS </font></p>
			<?php
			if (!isset($language) ||  !isset($proper) || !isset ($proper))
				echo "As per the uploaded data.";
			else{
				echo"<p> <strong>LANGUAGE: </strong>". ucfirst($language)."<p/>";
				if($proper ==1) $disp ='Excluded'; elseif($proper ==0) $disp ='Not excluded'; else $disp= 'Not defined for the selected language';
				echo"<p><strong> PROPER NOUNS: </strong>$disp</p>";
				if($periphrastic ==1) $disp ='Identified'; elseif($periphrastic ==0) $disp ='Not identified'; else $disp= 'Not defined for the selected language';
				echo"<p><strong> PERIPHRASTIC MORPHEMES: </strong>$disp</p>";
			}
			?>
			
			
			<hr/>
			<p> <font color = "blue"> VERBS AND NOUNS IN TEXT</font></p>
			<div class="scroll"><?php pos_colour_display($text);?></div>
			<br/>
			
			
			<form action="upload.php" method="post" enctype="multipart/form-data">
				<input type=button onClick="parent.location='CODE/save.php'" value='Download'>
			</form>
			
					
			
			<hr/>
			<p> <font color = "blue"> VERBS </font></p>
			<?php //print_r($text);?>
			<div id="Result1">
			
		
			<form name="Form1" action="" method="post" accept-charset="UTF-8" onsubmit="xmlhttpPost('CODE/calculate.php', 'Form1', 'Result1', '<img src=\'style/loading.gif\'>'); return false;">
			<p><strong>All verb exponences in text: </strong></p>
				<textarea rows="5" style="width:100%" name="morphemes_V"><?php echo(implode(", ", $morphology_V));?></textarea>
				<br/>
				
				<br/>
			
				
		
			<p> <font color = "blue"> NOUNS </font></p>
				<p><strong>All noun exponences in text: </strong></p>
		
							
				<textarea rows="5" style="width:100%" name="morphemes_N"><?php echo(implode(", ", $morphology_N));?></textarea>
				<br/>
				<hr/>
			<p> <font color = "blue"> PARAMETERS </font></p>
				
					
				
				segment size <input type="number" name="segments" size = "2" min="5" max="100" value = "10"> 
				random trials <input type="number" name="trials" size = "2" min="1" max="1000" value = "100"> 
				<br/>
				<br/>
				
				
				<input type="submit" value="Calculate MCI" />
				<br/>
			
			</form>
				
						
			<hr/>
		</div>
			<p><strong><a href="docs\about_MC.pdf"> How is morphological complexity (MC) is computed? </a></strong></p>
			<br/>
			<p><strong>How to cite?</strong></p>
			<p>Brezina, V. & Pallotti, G. (2015) "Morphological complexity tool", available from http://corpora.lancs.ac.uk/vocab/analyse_morph.php</p>
			<p>The analysis is based on: Pallotti, G. (2015). <a href="http://slr.sagepub.com/content/31/1/117"> A simple view of linguistic complexity.<em></a> Second Language Research 31</em> (1), pp. 117-134.</p>
			<br/>
			
		
				
		
		
	</div>
	
	<!-- Body end -->
	<div id="footer">
		<!-- Footer start -->
		<p> <?php footer() ?></a> </p>
		<!-- Footer end -->
	</div>
</div>
</body>