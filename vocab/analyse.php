<!DOCTYPE HTML>
<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>
			LancsLex: Lancaster vocab tool
		</title>
		<link rel="Index"         href="http://corpora.lancs.ac.uk/vocab/"     />
		<link  rel="stylesheet"   href="vocab1.css" type="text/css" media="all" />
		<link rel="shortcut icon" href="http://corpora.lancs.ac.uk/vocab/favicon.ico" />
		<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900,200italic,300italic,400italic,600italic,700italic,900italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
		<script src="clientside/show.js"></script>
		<script src="clientside/limit.js"></script>
		
			<script>
			  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			  ga('create', 'UA-49452093-2', 'lancs.ac.uk');
			  ga('send', 'pageview');

			</script>	
	
	</head>
<?php include 'CODE/functions.php'; ?>
<body>	
<div id="container">
		<?php header_page() ?>
	<div id="body">
		<!-- Body start -->
		<h1 id="heading">Analyse your texts with the <em>new-GSL</em></h1>
		<h2 id="subheading">English vocabulary interactive resource</h2>
		<hr/>
		
		<br/>
		<form method="post" action="process.php" accept-charset="UTF-8">
	<p>Paste the text you want to analyse into the text box below. <input readonly type="text" name="countdown" size="5" value="500000"> characters left.</p><br/>


		
		<textarea rows="20" cols="100" name="data" onKeyDown="limitText(this.form.data,this.form.countdown,500000);" 
		onKeyUp="limitText(this.form.data,this.form.countdown,500000);"></textarea><br/>
		
			<input type="checkbox" name="proper" value="1" checked>filter proper nouns
			<input type="checkbox" name="number" value="1" checked>filter numbers 
			<input type="checkbox" name="american" value="1">include American supplement 
		<br/>
		<br/>
		<input type="submit" value="Analyse text now" />
		<input type="reset" value="Clear" />
		

		</br>
		</br>
		
		
		
		
		<!-- Body end -->
	</div>
	<div id="footer">
		<!-- Footer start -->
		<p> <?php footer() ?></a> </p>
		<!-- Footer end -->
	</div>
</div>
</body>