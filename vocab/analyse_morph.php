<!DOCTYPE HTML>
<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>
			LancsLex: Lancaster vocab tool
		</title>
		<link rel="Index"         href="http://corpora.lancs.ac.uk/vocab/"     />
		<link  rel="stylesheet"   href="vocab1.css" type="text/css" media="all" />
		<link rel="shortcut icon" href="http://corpora.lancs.ac.uk/vocab/favicon.ico" />
		<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900,200italic,300italic,400italic,600italic,700italic,900italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
		<script src="clientside/show.js"></script>
		<script src="clientside/show.js"></script>
		<script src="clientside/limit.js"></script>
		
			<script>
			  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			  ga('create', 'UA-49452093-2', 'lancs.ac.uk');
			  ga('send', 'pageview');

			</script>	
	
	</head>
<?php include 'CODE/functions_morph.php'; include 'CODE/functions.php';?>
<body>	
<div id="container">
		<?php header_page() ?>
	<div id="body">
		<!-- Body start -->
		<h1 id="heading">Analyse your texts with <em>Morpho complexity tool</em></h1>
		<h2 id="subheading">Alpha version</h2>
		<hr/>
		<p> PLEASE NOTE: This tool is still under development and is not intended for general use yet. Current major limitations: 1) Analysis of nouns and periphrastic morphemes hasn’t been systematically implemented; 2) analysis for English is still very preliminary and that for Italian is based on theoretical models which will be revised soon; 3) analysis for German, French and Spanish hasn’t been implemented yet.</p>
		<p>The mathematical computation of the Morphological Complexity Index (MCI) can be considered more stable and can be used by inputting any list of morphological exponences in the boxes in the following page and then hitting the ‘calculate MCI’ button.</p>

		<br/>
		<form method="post" action="process_text_morph.php" accept-charset="UTF-8">
	<p>1. Paste the text you want to analyse into the text box below. <input readonly type="text" name="countdown" size="5" value="500000"> characters left.</p><br/>


		
		<textarea rows="20" cols="100" name="data" onKeyDown="limitText(this.form.data,this.form.countdown,500000);" 
		onKeyUp="limitText(this.form.data,this.form.countdown,500000);"></textarea><br/>
		
			
		<br/>
		2. Select language:
		<select name="language">
				<option value="english">English</option>
				<option value="french">French</option>
				<option value="german">German</option>
				<option value="italian">Italian</option>
				<option value="spanish">Spanish</option>
		</select>
		
		<br/>
		<br/>
		3. Choose settings options:
		<input type="checkbox" name="proper" value="1" checked> exclude proper nouns 
		<input type="checkbox" name="periphrastic" value="1" > identify periphrastic morphemes
		
		<br/>	
		<br/>	
		
		<input type="submit" value="Analyse text now" />
		<input type="reset" value="Clear" />
		</form>
		<hr/>
		<p>OR UPLOAD DATA FILE (csv)</p>
		
		<form action="upload.php" method="post" enctype="multipart/form-data">
			<input type="file" name="fileToUpload" id="fileToUpload" accept=".csv">
			<input type="submit" value="Upload" name="submit">
		</form>
		<form action="batch.php" method="post" enctype="multipart/form-data">
			<input type="file" name="fileToUpload" id="fileToUpload" accept=".csv">
			<input type="submit" value="Batch process" name="submit">
		</form>
		
		</br>
		</br>
		
		
		
		
		<!-- Body end -->
	</div>
	<div id="footer">
		<!-- Footer start -->
		<p> <?php footer() ?></a> </p>
		<!-- Footer end -->
	</div>
</div>
</body>