import { Component, OnInit } from '@angular/core';

import { ApiService } from '../api.service';

@Component({
	selector: 'app-analysis',
	templateUrl: './analysis.component.html',
	styleUrls: ['./analysis.component.css']
})
export class AnalysisComponent implements OnInit {
	// pie chart data
	public pieDatasets: any[];
	public pieLabels: string[];
	public pieOptions: any;
	
	// bar chart data
	public barDatasets: any[];
	public barLabels: string[];
	public barOptions: any;
	
	public analysisItems: any[];
	
	public showBreakdown: boolean = false;
	
	
	constructor(public api: ApiService) { }
	
	ngOnInit() {
		if (this.api.analysisSet) {
			// PIE CHART
			this.pieDatasets = [{
				'label': 'Text Coverage',
				'data': []
			}];
			this.pieLabels = [];
			for (let obj of this.api.analysis.pie) {
				this.pieDatasets[0]['data'].push(obj[1]);
				this.pieLabels.push(obj[0]);
			}
			
			// BAR CHART
			this.barDatasets = [{
				'label': 'Frequency for new-GSL Groups',
				'data': []
			}];
			this.barLabels = [];
			let barTooltips: any[] = [];
			for (let group of this.api.analysis.data) {
				this.barDatasets[0]['data'].push(group['value1']);
				this.barLabels.push(group['title']);
				barTooltips.push({'title': group['title1'], 'value': group['value']});
			}
			let analysisItemsTmp: any[] = [];
			analysisItemsTmp.push({'heading': 'First', 'subtext': 'First 500 words', 'data': this.api.analysis.first, 'display': false});
			analysisItemsTmp.push({'heading': 'Second', 'subtext': '500 - 1000 words', 'data': this.api.analysis.second, 'display': false});
			analysisItemsTmp.push({'heading': 'Third', 'subtext': '1000 - 2500 words', 'data': this.api.analysis.third, 'display': false});
			analysisItemsTmp.push({'heading': 'Last', 'subtext': 'Not in the new-GSL', 'data': this.api.analysis.fourth, 'display': false});
			this.analysisItems = [];
			console.log(analysisItemsTmp);
			for (let analysisItem of analysisItemsTmp) {
				for (let idx in analysisItem.data) {
					console.log(analysisItem.data[idx]);
					analysisItem.data[idx] = analysisItem.data[idx].replace(/^[A-Z]_/g, '');
					console.log(analysisItem.data[idx]);
				}
				analysisItem.data_ = analysisItem.data.sort();
				analysisItem.data = analysisItem.data_.join(', ');
				this.analysisItems.push(analysisItem);
			}
			
			this.barOptions = {
				tooltips: {
					callbacks: {
						label: (tooltipItem) => {
							console.log(tooltipItem);
						}
					}
				},
				onClick: (e: any) => {
					if (e.active.length > 0) {
						const chart = e.active[0]._chart;
						const activePoints = chart.getElementAtEvent(e.event);
						if (activePoints.length > 0) {
							const clickedElementIndex = activePoints[0]._index;
							if (this.analysisItems[clickedElementIndex].display) {
								this.analysisItems[clickedElementIndex].display = false;
								this.showBreakdown = false;
							} else {
								for (let analysisItem of this.analysisItems)
									analysisItem.display = false;
								this.analysisItems[clickedElementIndex].display = true;
								this.showBreakdown = true;
							}
						}
					}
				}
			};
			
		} // end if else
	}
	
}
