<!DOCTYPE HTML>
<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>
			LancsLex: Lancaster vocab tool
		</title>
		<link rel="Index"         href="http://corpora.lancs.ac.uk/vocab/"     />
		<link  rel="stylesheet"   href="vocab1.css" type="text/css" media="all" />
		<link rel="shortcut icon" href="http://corpora.lancs.ac.uk/vocab/favicon.ico" />
		<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900,200italic,300italic,400italic,600italic,700italic,900italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
		<script src="clientside/show.js"></script>	
			<script>
			  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			  ga('create', 'UA-49452093-2', 'lancs.ac.uk');
			  ga('send', 'pageview');

			</script>
	
	
	</head>
<?php include 'CODE/functions.php'; ?>
<body>	
<div id="container">
		<?php header_page() ?>
	<div id="body">
		<!-- Body start -->
		<h1 id="heading">Browse the <em>new-GSL</em></h1>
		<h2 id="subheading">English vocabulary interactive resource</h2>
		<hr/>
		<div> Here you can browse different versions of the <em>new-GSL:</em>  
			<ul>
				<li type="square"> <a href="http://applij.oxfordjournals.org/content/early/2013/08/25/applin.amt018/suppl/DC1" target="_blank">Alphabetical list [published version]</a> </li>
				<li type="square"> <a href="docs/new_GSL_rank.pdf"  target="_blank"> Frequency list [rank] </a> </li>
			</ul>
		</div> 
		<div>
			The article from <em> Applied Linguistics </em>that describes the creation of the <em>new-GSL</em> is available from <a href="http://applij.oxfordjournals.org/content/early/2013/08/25/applin.amt018.full"  target="_blank"> this link</a> [open access].
		</div> 
		<!-- Body end -->
	</div>
	<div id="footer">
		<!-- Footer start -->
		<p> <?php footer() ?></a> </p>
		<!-- Footer end -->
	</div>
</div>
</body>