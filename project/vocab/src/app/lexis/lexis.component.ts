import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';

import { ApiService } from '../api.service';

@Component({
	selector: 'app-lexis',
	templateUrl: './lexis.component.html',
	styleUrls: ['./lexis.component.css']
})
export class LexisComponent implements OnInit {
	public msg: string = '';
	
	public charLim: number = 500000;
	public text: string = '';
	
	public nounChecked: boolean = true;
	public numbersChecked: boolean = true;
	public americanChecked: boolean = false;
	
	public tAreaFocus: boolean = false;
	
	constructor(private api: ApiService,
				private router: Router) { }
	
	ngOnInit() {
	}
	
	public limitCharLength(): void {
		if (this.text.length > this.charLim) {
			this.text = this.text.substring(0, this.charLim);
		}
	}
	
	public submitAnalysis(): void {
		this.api.analyse(this.text, this.nounChecked, this.numbersChecked, this.americanChecked).subscribe((res: any) => {
			console.log(res);
			if (res.type == 0) {
				this.msg = res.msg;
				
			} else {
				this.api.analysis = res.msg;
				this.api.analysisSet = true;
				this.router.navigate(['analysis']);
			}
		});
	}
	
}
