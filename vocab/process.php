<?php
ini_set('display_errors', '1');
chdir('CODE');

include 'functions.php';
include 'new_GSL.php';
include 'chart_all.php';

				
if (function_exists('mb_strlen'))
{
	mb_internal_encoding("UTF-8");
	$len = 'mb_strlen';
	$sub = 'mb_substr';
}
else
{
	$len = 'strlen';
	$sub = 'substr';
}

//Is data passed via $_POST?
if (!isset($_POST['data']))
{
	echo "ERROR: Your text could not be processed because the request was badly-formed, please try again.";
	exit;
}

$data = $_POST['data'];


$proper = 0;
$number = 0;
$supplement_us = 0;

if (isset ($_POST['proper']))
	$proper = (int)$_POST['proper'];
if (isset($_POST['number']))
	$number = (int)$_POST['number'];
if (isset($_POST['american']))
	$supplement_us = (int)$_POST['american'];
/*ADD SUPPLEMENTS*/	
if ($supplement_us == 1){
	$new_GSL_nouns = array_merge($new_GSL_nouns, $new_GSL_nouns_us);
	$new_GSL_verbs = array_merge($new_GSL_verbs, $new_GSL_verbs_us);
	$new_GSL_adj = array_merge($new_GSL_adj, $new_GSL_adj_us);
	$new_GSL_adv = array_merge($new_GSL_adv, $new_GSL_adv_us);
	$new_GSL_pron = array_merge($new_GSL_pron, $new_GSL_pron_us);
	$new_GSL_con = array_merge($new_GSL_con, $new_GSL_con_us);
	$new_GSL_other = array_merge($new_GSL_other, $new_GSL_other_us);
}





//Check the length of the input & trim
if ($len($data) > 500000)
{
	$i = 500000;
	while ( false === strpos(" \t\r\n", $sub($data, $i, 1)))
		$i--;
	$data = $sub($data, 0, $i);
}

//Pre-process data
$data = str_replace("\r\n", "\n", $data);


//Start tagging
$in = tempnam('/tmp', 'WebTreeTag');
file_put_contents($in, $data);
$out = "$in.vrt";
$language = "english";
system("TreeTagger $language $in");



if (false !== strpos($_SERVER['HTTP_USER_AGENT'], 'Windows'))
	system("unix2dos $out");
	
	

//Process after tagging and create a new temp file

$inx = tempnam('/tmp', 'processed_file');
$inx1 = "$inx.vrt";
$source= fopen ($out, "r");
$output= fopen ($inx1, "w");


while (false!==($line=fgets($source))) {
	$new_line = process($line, $proper, $number);
	fwrite($output,$new_line);
}
fclose ($source);
fclose ($output);

/*
//local run	
$inx1 = "test.vrt";
set_time_limit(60);
$out = "test.vrt";
$inx = 'N_'.$out;
$source= fopen ("test.vrt", "r");
$output= fopen ($inx, "w");
while (false!==($line=fgets($source))) {
	$new_line = process($line, $proper, $number);
	fwrite($output,$new_line);
}

//end local

*/



//summary of coverage
$source= fopen($inx1, "r");//$inx1 in online version
$lexicon = array();

$display_text = array();


while (false!==($line=fgets($source))){
		$words = preg_split("/\r\n/" , trim($line)); //split strings
		foreach ($words as $w) //do something for each value in an array
		{
					
		
			$w1= preg_replace('/^.*_([A-Z]*_.*\s?$)/','$1',$w);//cutting unwanted bits and leaving in last two parts
			//if (preg_match('/[\W\.\?\!].*/',$w1) =="1")// skip strings that start with...
			//continue;
			
			$gsl = find_gsl_status($w1);
			//$w = strtolower($w);
			if (array_key_exists ($w1, $lexicon))
				$lexicon [$w1]++;
			else
				$lexicon [$w1] = 1; //first occurrence
				
			$w2= preg_replace('/(^.*)_.*_.*\s?$/','$1',$w);//leaving in only first part
				$display_text [] = $w2.$gsl; //attach gsl info about the word
				
				
			
		}
}
		

fclose($source);
arsort ($lexicon);




	
//delete temporary files

unlink($in);
unlink($out);
unlink ($inx);
unlink ($inx1);

/*display the frequency list
foreach ($lexicon as $type => $freq)
	echo '<font color="red">'.$type.'</font> '.$freq.'<br>';
*/
//set 1 as default to avoid division errors



//count words in text
$nouns_in_text = count_POS_in_text_and_get_items($lexicon, 'N');
$verbs_in_text = count_POS_in_text_and_get_items($lexicon, 'V');
$mod_in_text = count_POS_in_text_and_get_items($lexicon, 'M');
$pron_in_text = count_POS_in_text_and_get_items($lexicon, 'P');
$adj_in_text = count_POS_in_text_and_get_items($lexicon, 'J');
$adv_in_text = count_POS_in_text_and_get_items($lexicon, 'R');
$con_in_text = count_POS_in_text_and_get_items($lexicon, 'C');
$other_in_text = count_POS_in_text_and_get_items($lexicon, 'X');
$all_in_text = $nouns_in_text[0] + $verbs_in_text[0] + $mod_in_text[0] + $pron_in_text[0] + $adj_in_text[0] + $adv_in_text[0] + $con_in_text[0] + $other_in_text[0];

//count new-GSL 500


$nouns_500 = get_word_class_counts_and_items($new_GSL_nouns, $lexicon, 'N', 1);
$verbs_500 = get_word_class_counts_and_items($new_GSL_verbs, $lexicon, 'V', 1);
$mod_500 = get_word_class_counts_and_items($new_GSL_mod, $lexicon, 'M', 1);
$adj_500 = get_word_class_counts_and_items($new_GSL_adj, $lexicon, 'J', 1);
$adv_500 = get_word_class_counts_and_items($new_GSL_adv, $lexicon, 'R', 1);
$con_500 = get_word_class_counts_and_items($new_GSL_con, $lexicon, 'C', 1);
$pron_500 = get_word_class_counts_and_items($new_GSL_pron, $lexicon, 'P', 1);
$other_500 = get_word_class_counts_and_items($new_GSL_other, $lexicon, 'X', 1);

//count 1000
$nouns_1000 = get_word_class_counts_and_items($new_GSL_nouns, $lexicon, 'N', 2);
$verbs_1000 = get_word_class_counts_and_items($new_GSL_verbs, $lexicon, 'V', 2);
$mod_1000 = get_word_class_counts_and_items($new_GSL_mod, $lexicon, 'M', 2);
$adj_1000 = get_word_class_counts_and_items($new_GSL_adj, $lexicon, 'J', 2);
$adv_1000 = get_word_class_counts_and_items($new_GSL_adv, $lexicon, 'R', 2);
$con_1000 = get_word_class_counts_and_items($new_GSL_con, $lexicon, 'C', 2);
$pron_1000 = get_word_class_counts_and_items($new_GSL_pron, $lexicon, 'P', 2);
$other_1000 = get_word_class_counts_and_items($new_GSL_other, $lexicon, 'X', 2);


//count 2500
$nouns_2500 = get_word_class_counts_and_items($new_GSL_nouns, $lexicon, 'N', 3);
$verbs_2500 = get_word_class_counts_and_items($new_GSL_verbs, $lexicon, 'V', 3);
$mod_2500 = get_word_class_counts_and_items($new_GSL_mod, $lexicon, 'M', 3);
$adj_2500 = get_word_class_counts_and_items($new_GSL_adj, $lexicon, 'J', 3);
$adv_2500 = get_word_class_counts_and_items($new_GSL_adv, $lexicon, 'R', 3);
$con_2500 = get_word_class_counts_and_items($new_GSL_con, $lexicon, 'C', 3);
$pron_2500 = get_word_class_counts_and_items($new_GSL_pron, $lexicon, 'P', 3);
$other_2500 = get_word_class_counts_and_items($new_GSL_other, $lexicon, 'X', 3);

//bands
$all_500 = $nouns_500[0] + $verbs_500[0] + $mod_500[0] + $adj_500[0] + $adv_500[0] + $con_500[0] + $pron_500[0] + $other_500[0];
$all_1000 = $nouns_1000[0] + $verbs_1000[0] + $mod_1000[0] + $adj_1000[0] + $adv_1000[0] + $con_1000[0] + $pron_1000[0] + $other_1000[0];
$all_2500 = $nouns_2500[0] + $verbs_2500[0] + $mod_2500[0] + $adj_2500[0] + $adv_2500[0] + $con_2500[0] + $pron_2500[0] + $other_2500[0];


//bands items
session_start();
$_SESSION['a'] =array_merge($nouns_500[1], $verbs_500[1], $mod_500[1], $adj_500[1], $adv_500[1], $con_500[1], $pron_500[1], $other_500[1]);
$_SESSION['b'] =array_merge($nouns_1000[1], $verbs_1000[1], $mod_1000[1], $adj_1000[1], $adv_1000[1], $con_1000[1], $pron_1000[1], $other_1000[1]);
$_SESSION['c'] =array_merge($nouns_2500[1], $verbs_2500[1], $mod_2500[1], $adj_2500[1], $adv_2500[1], $con_2500[1], $pron_2500[1], $other_2500[1]);

//all word classes

$nouns_all = $nouns_500[0] + $nouns_1000[0] + $nouns_2500[0];
$verbs_all = $verbs_500[0] + $verbs_1000[0] + $verbs_2500[0];
$mod_all = $mod_500[0] + $mod_1000[0] + $mod_2500[0];
$adj_all = $adj_500[0] + $adj_1000[0] + $adj_2500[0];
$adv_all = $adv_500[0] + $adv_1000[0] + $adv_2500[0];
$con_all = $con_500[0] + $con_1000[0] + $con_2500[0];
$pron_all = $pron_500[0] + $pron_1000[0] + $pron_2500[0];
$other_all = $other_500[0] + $other_1000[0] + $other_2500[0];

$all_coverage = $nouns_all + $verbs_all + $mod_all + $adj_all + $adv_all + $con_all + $pron_all + $other_all;
$all_coverage_per_cent = round(($all_coverage/$all_in_text * 100),2);


$nouns_coverage_per_cent = calculate_coverage ($nouns_all,$nouns_in_text[0]);
$verbs_coverage_per_cent = calculate_coverage($verbs_all,$verbs_in_text[0]);
$mod_coverage_per_cent = calculate_coverage($mod_all,$mod_in_text[0]);
$adj_coverage_per_cent = calculate_coverage($adj_all,$adj_in_text[0]);
$adv_coverage_per_cent = calculate_coverage($adv_all,$adv_in_text[0]);
$con_coverage_per_cent = calculate_coverage($con_all,$con_in_text[0]);
$pron_coverage_per_cent = calculate_coverage($pron_all,$pron_in_text[0]);
$other_coverage_per_cent = calculate_coverage($other_all,$other_in_text[0]);


$nouns_offlist = get_offlist_counts_and_items($new_GSL_nouns, $lexicon, 'N');
$verbs_offlist = get_offlist_counts_and_items($new_GSL_verbs, $lexicon, 'V');
$mod_offlist = get_offlist_counts_and_items($new_GSL_mod, $lexicon, 'M');
$adj_offlist = get_offlist_counts_and_items($new_GSL_adj, $lexicon, 'J');
$adv_offlist = get_offlist_counts_and_items($new_GSL_adv, $lexicon, 'R');
$con_offlist = get_offlist_counts_and_items($new_GSL_con, $lexicon, 'C');
$pron_offlist = get_offlist_counts_and_items($new_GSL_pron, $lexicon, 'P');
$other_offlist = get_offlist_counts_and_items($new_GSL_other, $lexicon, 'X');
$all_offlist = $nouns_offlist[0] + $verbs_offlist[0] + $mod_offlist[0] + $adj_offlist[0] + $adv_offlist[0] + $con_offlist[0] + $pron_offlist[0] + $other_offlist[0];
$_SESSION['d'] =array_merge($nouns_offlist[1], $verbs_offlist[1], $mod_offlist[1], $adj_offlist[1], $adv_offlist[1], $con_offlist[1], $pron_offlist[1], $other_offlist[1]);


// sort out sentences
if(!empty($lexicon['SENT_.']))
	$sent1 = $lexicon['SENT_.']; 
else
	$sent1 = 0;

if(!empty($lexicon['SENT_?']))
	$sent2 = $lexicon['SENT_?']; 
else
	$sent2 = 0;
	
if(!empty($lexicon['SENT_!']))
	$sent3 = $lexicon['SENT_!']; 
else
	$sent3 = 0;	

$sentences = 0 + $sent1+ $sent2 + $sent3;

$_SESSION['e'] = $display_text;

$data2 = array();
/**************** F I L L   D A T A   A R R A Y *******************************/

$data2[0]['title'] = 'new-GSL 500';
$data2[0]['title1'] = 'top 500 English words';
$data2[0]['part'] = 'First';
$data2[0]['value'] = $all_500;
$data2[0]['value1'] = round(($all_500/$all_in_text*100),1);
$data2[1]['title'] = 'new-GSL 1000';
$data2[1]['title1'] = '500-1000 English words';
$data2[1]['part'] = 'Second';
$data2[1]['value'] = $all_1000;
$data2[1]['value1'] = round(($all_1000/$all_in_text*100),1);
$data2[2]['title'] = 'new-GSL 2500';
$data2[2]['title1'] = '1000-2500 English words';
$data2[2]['part'] = 'Third';
$data2[2]['value'] = $all_2500;
$data2[2]['value1'] = round(($all_2500/$all_in_text*100),1);
$data2[3]['title'] = 'Off list';
$data2[3]['title1'] = 'Words not in the new-GSL';
$data2[3]['part'] = 'Last';
$data2[3]['value'] = $all_offlist;
$data2[3]['value1'] = round(($all_offlist/$all_in_text*100),1);

		
		$image = "style/img_blue.gif";
		 

include 'display_analysis.php';


?>