<?php


/*** morphology related ***/

/*
function periphrastic_morphemes($value, $array){
	$count=0;
if($value == ""///if the POS is on periphratic list candidate

}

*/

function analyze_morphology($f, $stem, $irregularities, $umlaut, $umlauts) {
	
	//isolate word form (WF) output and convert to lower case
	$f1 = trim(preg_replace('/(^.*?)\t[VMN].*?\t.*?\s?$/','$1', $f));
	$f1 = strtolower($f1);
	
	
	//isolate citation form (CF) and convert to lower space
	$f2 = trim(preg_replace('/^.*?\t[VMN].*?\t(.*?)\s?$/','$1', $f));
	$f2 = strtolower($f2);
	
	//print_r($f2);
	
	//print_r($irregularities[0]);
	
	//$stem = 1;
	
	//stem switch on
	if($stem==1){
		//PRE-PROCESSING: deal with suppletives and other irregularities
		foreach ($irregularities[0] as $key => $value){
			
			if(preg_match($irregularities[0][$key][0], $f))
				return $irregularities[0][$key][1];
				
		}
			
		//PRE-PROCESSING: deal with IW (word forms)
		foreach ($irregularities[1] as $key => $value){
			//echo $irregularities[1][$key][0];
			if(preg_match($irregularities[1][$key][0], $f)){
				$f1 = preg_replace($irregularities[1][$key][1], $irregularities[1][$key][2],$f1);
				echo"IW:$f1";
			}
		}
		
		
		//PRE-PROCESSING: deal with CFS (headwords)
		foreach ($irregularities[2] as $key => $value){
			//echo $irregularities[2][$key][0];
			//echo"\r\n";
			if(preg_match($irregularities[2][$key][0], $f)){
				//echo"YES\r\n";
				$f2 =  preg_replace($irregularities[2][$key][1], $irregularities[2][$key][2],$f2);
				//echo "CF: $f2; ";
			}
		}
		
		
		/*		
		//deal with CF (headwords)	
		if(preg_match($irregularities[4], $f))
			$f2 = (preg_replace($irregularities[5], $irregularities[6], $f2));
		*/				
	}
	
	//run comparison - standard procedure	
	if ($f1 == $f2)
		return 'Ø';
	elseif(strpos($f1,$f2) !== false) {
		$exponent = str_replace($f2,'', $f1);
		//POST-PROCESS EXPONENT
		foreach ($irregularities[3] as $key => $value){
			//echo $irregularities[2][$key][0];
			if(preg_match($irregularities[3][$key][0], $f)){
				$exponent =  preg_replace($irregularities[3][$key][1], $irregularities[3][$key][2],$exponent);
				
			}
		}
		
		return $exponent;
	}
	//try again with umlauts
	else{
		//POST-PROCESSING: apply list of endings
		
		foreach ($irregularities[4] as $key => $value){
			//echo $irregularities[4][$key][0];
			if(preg_match($irregularities[4][$key][0], $f)){
				$exponent =  preg_replace($irregularities[4][$key][1], $irregularities[4][$key][2],$f1);
				echo"F1: $f1 - ex: $exponent";
				return $exponent;
			}
		}
		return $f1;
	
	}
	

}



function find_stem($a, $b)
{
    $r = '';
    $len = strlen($a) > strlen($b) ? strlen($b) : strlen($a);
    for($i=0; $i<$len; $i++)
    {
        if(substr($a, $i, 1) == substr($b, $i, 1))
        {
            $r .= substr($a, $i, 1);
        }
        else
        {
            break;
        }
    }
    $ending = strlen($b) - strlen($r);
	if($ending > 4 || (strlen($b) - $ending) <= 2)
		$result = $b;
	else
		$result = $r;
	return $result;
}



function return_array_x_multiplier ($array, $multiplier){
    $count = count($array);
	$trim = (floor($count/$multiplier))*$multiplier;
	$new_array = (array_slice($array, 0, $trim));
    return $new_array;
	};


function count_exponents_for_subsamples($array){
	$total = 0;
	foreach ($array as $value){
		$total = $total + count(array_count_values($value));

	}
	$count = count($array);
	if($count>0)
		$average = $total/$count;
	else $average =0;
	return $average;	
}

function calculate_average_dissimilarity($array){
	//compare each value with the rest
	$total = 0;
	$j=0;
	foreach ($array as $key => $value){
		$i = $key +1;
		while($i<count($array)){
			$total = $total + count(array_merge (array_diff(array_unique($value), array_unique($array[$i])),array_diff(array_unique($array[$i]), array_unique($value))));
			$i++;
			$j++;
		}
	}
	if($j>0)
		$average = $total/$j;
	else $average =0;
	return $average;
}

//run random trials
function run_random_trials($array, $segments, $count){
	$i = 0;
	$average_exponents_all = 0;
	$dissimilarity_all = 0;
	while ($i<$count){
		shuffle($array); 
		$array1 = return_array_x_multiplier($array, $segments);
		$array1 = (array_chunk($array1, $segments)); // divide into sub-arrays x elements each
		$average_exponents = (count_exponents_for_subsamples($array1));
		$dissimilarity = (calculate_average_dissimilarity($array1));
		$average_exponents_all = $average_exponents_all + $average_exponents; 
		$dissimilarity_all = $dissimilarity_all + $dissimilarity;
		$i++;
	}
	$average_exponents_all = $average_exponents_all/$count;
	$dissimilarity_all = $dissimilarity_all/$count;
	return(array($average_exponents_all, $dissimilarity_all));
}





function calculate_complexity($morphology, $segments, $trials) {
	$morphology0 = $morphology; // state before any processing 
	$morphology_random = $morphology;
	$random_average = run_random_trials($morphology,$segments, $trials);
	
	shuffle($morphology_random); 
	$morphology = return_array_x_multiplier($morphology, $segments);
	$morphology_random = return_array_x_multiplier($morphology_random, $segments);
	//print_r($morphology);
	$morphology1 = $morphology; // array with elements for analysis dividable by $segments
	$morphology2 = $morphology_random; // random array with elements for analysis dividable by $segments

	$morphology = (array_chunk($morphology, $segments)); // divide into sub-arrays $segments elements each
	$morphology_random = (array_chunk($morphology_random, $segments));
	//print_r ($morphology);
	
	$average_exponents = (count_exponents_for_subsamples($morphology));
	$average_exponents_random = (count_exponents_for_subsamples($morphology_random));
	$dissimilarity = (calculate_average_dissimilarity($morphology));
	$dissimilarity_random = (calculate_average_dissimilarity($morphology_random));
	$morphology_diplay = array($average_exponents, $dissimilarity, '', '', $average_exponents_random, $dissimilarity_random,'', $random_average);
	return $morphology_diplay;
}


function pos_colour_display($array){

	foreach($array as $value){
	
		if(preg_match('/^(?!(n\'t|\'re|\'m|\'s))\w.*/',$value))
			$space=" ";
		elseif(preg_match('/^\(.*/',$value))
			$space=" ";
		elseif(preg_match('/^\[.*/',$value))
			$space=" ";
		elseif(preg_match('/^\{.*/',$value))
			$space=" ";
		elseif(preg_match('/^\".*/',$value))
			$space=" ";
		elseif(preg_match('/^\).*/',$value))
			$space=" ";
		elseif(preg_match('/^\].*/',$value))
			$space=" ";
		elseif(preg_match('/^\}.*/',$value))
			$space=" ";
		else
			$space ="";
		
		$word = preg_replace('/(^.+?)\t.+?\t.+?\t.*\s?$/','$1',$value);
		$word1 = preg_replace('/(^.+?)\t.+?\t.+?\s?$/','$1',$value);
		
	
		if (preg_match('/^.+?\t[N].+?\t.+?\t.*\s?$/',$value))
			echo$space.'<span style="font-weight: bold;color:blue;" title="noun">'.$word.'</span>';
		elseif (preg_match('/^.+?\t[VM].+?\t.+?\t.*\s?$/',$value))
			echo$space.'<span style="font-weight: bold;color:red;" title="verb">'.$word.'</span>';
		elseif (preg_match('/(^.+?)\t.+?\t.+?\s?$/',$value))
			echo$space.$word1;
		
		
	}
}


//calculate STTR

function calculate_STTR ($array, $segments, $trials){
	$i = 0;
	$sttr_all = 0;
	while ($i<$trials){
		shuffle($array);
		$array1 = return_array_x_multiplier($array, $segments);
		$array1 = (array_chunk($array1, $segments)); // divide into sub-arrays x elements each
		//print_r($array1);
		$sttr=0;
		foreach($array1 as $value){
			 $sttr = $sttr + (count(array_count_values($value)))/$segments;
		
		}
		
		$sttr_all = $sttr_all+ $sttr/count($array1);
		
		$i++;
	}
	$sttr_all = $sttr_all/$trials;
	return $sttr_all;
	
		
}

///sanitise textbox input with the X,y, X pattern

function sanitise_morph_box($string){
	 
	$pattern="/^[A-Za-ěščřžzàèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝâêîôûÂÊÎÔÛãñõÃÑÕäëïöüÿÄËÏÖÜŸçÇßØøÅåÆæœ\s,'-_]+$/"; 
	if (preg_match($pattern, $string)==1)
		;
	else{
		echo "ERROR: Your input does not have an expected format, please go back, correct the input and try again.";
		exit;
	}
}


?>