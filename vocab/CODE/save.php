<?php
session_start();
$text = $_SESSION['h'];
//print_r($text);

header('Content-type: text/plain');
header('Content-disposition: attachment; filename="complexity_output.csv"');
$output = fopen("php://output",'w');


$header ="Word_form,POS,Headword,V_Exponent,N_Exponent\r\n";
$footer ="\r\n\r\nSOURCE: Morphological analysis tool; Vaclav Brezina & Gabriele Pallotti 2015";

fputs($output, $header);

foreach ($text as $item){
	$item1 = preg_replace("/^(.+?\t[N].+?\t.+?\t)(.+?)$/","$1\t$2", $item );
	$item1 = preg_replace("/,\t,\t,/","\",\"\t\",\"\t\",\"", $item1 );
	$item1 = preg_replace('/\t/',',', $item1 );
	$item1 = trim($item1)."\r\n";
	fputs($output, $item1);
	//$item2 = preg_replace("/, /", "\t", $item ['title1'])."\t";
	//fputs($output, $item2);
	//$item3 = $item ['value']."\t";
	//fputs($output, $item3);
	//$item4 = $item ['value1']."\r\n";
	//fputs($output, $item4);
	}
//mb_convert_encoding($output, 'UTF-16LE', 'UTF-8');

fputs($output, $footer);

fclose($output);

?>