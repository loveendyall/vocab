<?php
ini_set('max_execution_time', 300);
include 'CODE/functions_morph.php';
include 'CODE/functions.php';
//print_r($_FILES);
$error = "";
if (isset($_FILES["fileToUpload"])) {
	$allowedExts = array("csv");
	$temp = explode(".", $_FILES["fileToUpload"]["name"]);
	$extension = end($temp);

	if ($_FILES["fileToUpload"]["error"] > 0) {
		$error .= "Error opening the file<br />";
	}
	//if ( $_FILES["fileToUpload"]["type"] != "text/plain") {	
		//$error .= "Mime type not allowed<br />";
	//}
	if (!in_array($extension, $allowedExts)) {
		$error .= "Extension not allowed<br />";
	}
	if ($_FILES["fileToUpload"]["size"] > 5120000) {
		$error .= "File size should be less than 1000 kB<br/>";
	}

	if ($error == "") {
		;
	} 
	else {
		echo $error;
		exit;
	}
}	
else{
	echo 'ERROR: File error';
	exit;
}

//echo "The tool started processing the data - this may take several minutes...";
/*
if (!isset ($_SESSION['i'])){
	echo "ERROR: Your input could not be processed because the request was badly-formed, please go back and try again.";
	exit;
}
*/
//define settings
//$language = $_SESSION['i'][0];
//$proper = $_SESSION['i'][1];
//$periphrastic = $_SESSION['i'][2];

// store file content as a string in $str
$str = file_get_contents($_FILES["fileToUpload"]["tmp_name"]);

$str = str_replace("\r\n", "\n", $str);


$str = ltrim($str, 'Word_form,POS Headword,V_Exponent,N_Exponent');
$str = rtrim($str, '\n');
//$str = rtrim($str, 'SOURCE: Morphological analysis tool; Vaclav Brezina & Gabriele Pallotti 2015');
//echo $str
$text = explode("\n", $str);



foreach ($text as &$value){
$value = preg_replace("/\",\"/","xxcxx", $value );
//$value = preg_replace("/,$/","", $value );
$value = preg_replace("/,/","\t", $value );
$value = preg_replace("/xxcxx/",",", $value );
$value = preg_replace("/SOURCE: Morphological analysis tool; Vaclav Brezina & Gabriele Pallotti 2015/","", $value);
}
//print_r($text);



$lexicon = array();
$sentences = array();
$morphology_V = array();
$morphology_N = array();


unlink($_FILES["fileToUpload"]["tmp_name"]);
		
		

$i = 0;
foreach($text as &$w){	
	if (preg_match('/<FILE_.*?>/', $w)){
		$i++;
		$x= preg_replace('/^<(.*?)>$/','_$1',$w);
		$lexicon[$i.$x] = array();
		$sentences[$i.$x] = "";
	}
	if (preg_match('/^.+?\t[N](?!U).+?\t.+?\t.+?\s?$/', $w)){
		$w1 = preg_replace('/^(.+?\t[N].+?\t.+?\t)(.+?)$/','$2', $w);
		$morphology_N[$i.$x][] = trim($w1);
	}
	if (preg_match('/^.+?\t[MV].*[^#]\t.+?\t.+?\s?$/', $w)){
		$w1 = preg_replace('/^(.+?\t[VM].+?\t.+?\t)(.+?)$/','$2', $w);
		$morphology_V[$i.$x][] = trim($w1);
	}
	
	if (preg_match('/^[A-Za-z].*?\t.*?\t.+?\s?$/', $w)){
		
		if (preg_match('/^.*?\t([A-Z]).*?\t(.*?)\t.*?$/', $w))
				$w1= preg_replace('/^.*?\t([A-Z]).*?\t(.*?)\t.*?$/','$1_$2',$w);//cutting unwanted bits and leaving in last two parts
		else
			$w1= preg_replace('/^.*?\t([A-Z]).*?\t(.*?)$/','$1_$2',$w);//cutting unwanted bits and leaving in last two parts
		
		$lexicon[$i.$x][] = trim($w1);
				
			
	}		
	if (preg_match('/.*?\[S_START\].*?$/', $w)){
			
		$sentences[$i.$x]++;
				
			
	}		
}

//print_r($sentences);


if (empty($morphology_V)||empty($morphology_N)) {
	echo'DATA ERROR: No morphemes were recognised.';
	exit;
}

if ($i==0) {
	echo'DATA ERROR: No valid file breaks were recognised.';
	exit;
}

$text = array_filter($text);
//print_r($sentences);
//print_r($text);

//$morphology_V = array ("ound", "X", "X", "X", "is", "X", "X", "X", "X", "ed", "fell", "was", "was", "X", "ound", "ound", "was", "X", "ound", "made", "fell", "are", "X", "fell", "X", "ed", "came", "was", "fell", "was", "ound", "X", "was", "X", "X");




//store text for download
//$_SESSION['h'] = $text;	
		
//print_r($morphology_N);
//print_r($morphology_V);

$trials = 1;

foreach ($morphology_V as $key => $value){
$morphology_V_diplay[$key] = calculate_complexity($morphology_V[$key], '10', $trials);	
$value1 = $morphology_V_diplay[$key][7][0];
$value2 = $morphology_V_diplay[$key][7][0] + (($morphology_V_diplay[$key][7][1]/2)-1);
$array_V[$key] = "$value1,$value2";
}

foreach ($morphology_N as $key => $value){
$morphology_N_diplay[$key] = calculate_complexity($morphology_N[$key], '10', $trials);
$value1 = $morphology_N_diplay[$key][7][0];
$value2 = $morphology_N_diplay[$key][7][0] + (($morphology_V_diplay[$key][7][1]/2)-1);
$array_N[$key] = "$value1,$value2";

}

foreach ($morphology_V as $key => $value){
$morphology_V_diplay[$key] = calculate_complexity($morphology_V[$key], '5', $trials);	
$value1 = $morphology_V_diplay[$key][7][0];
$value2 = $morphology_V_diplay[$key][7][0] + (($morphology_V_diplay[$key][7][1]/2)-1);
$array_V[$key] = $array_V[$key].",$value1,$value2";
}

foreach ($morphology_N as $key => $value){
$morphology_N_diplay[$key] = calculate_complexity($morphology_N[$key], '5', $trials);
$value1 = $morphology_N_diplay[$key][7][0];
$value2 = $morphology_N_diplay[$key][7][0] + (($morphology_V_diplay[$key][7][1]/2)-1);
$array_N[$key] = $array_N[$key].",$value1,$value2";

}

foreach ($lexicon as $key => $value){
$lexicon_diplay[$key] = calculate_STTR($lexicon[$key], '100', $trials);
$value1 = $lexicon_diplay[$key];
$value2 = count($lexicon[$key]);
$array_V[$key] = $array_V[$key].",$value2, $value1";
$array_N[$key] = $array_N[$key].",$value2,$value1";
}
//order MC10a MC10 MC5a MC5

foreach ($sentences as $key => $value){
$array_V[$key] = $array_V[$key].",$value";
$array_N[$key] = $array_N[$key].",$value";


}



//print_r($morphology_V_diplay);

//print_r($array_V[1]);


header('Content-type: text/plain');
header('Content-disposition: attachment; filename="complexity_batch_output.csv"');
$output = fopen("php://output",'w');


$header ="File,MC10a,MC10,MC5a,MC5,tokens, STTR_100_rand, sentences, tokens/sentence\r\n";
$footer ="\r\n\r\nSOURCE: Morphological analysis tool; Vaclav Brezina & Gabriele Pallotti 2015";

fputs($output, $header);

foreach ($array_V as $key=>$item){
	$item1 = $key."_V,".trim($item)."\r\n";
	fputs($output, $item1);
	//$item2 = preg_replace("/, /", "\t", $item ['title1'])."\t";
	//fputs($output, $item2);
	//$item3 = $item ['value']."\t";
	//fputs($output, $item3);
	//$item4 = $item ['value1']."\r\n";
	//fputs($output, $item4);
	}
//mb_convert_encoding($output, 'UTF-16LE', 'UTF-8');


foreach ($array_N as $key=>$item){
	$item1 = $key."_N,".trim($item)."\r\n";
	fputs($output, $item1);
}
fputs($output, $footer);

fclose($output);


//include 'analyse_morph.php';

/**
$results = print_r($array_V, true); 
file_put_contents('output_Vx.txt', $results);
$results1 = print_r($array_N, true); 
file_put_contents('output_Nxx.txt', $results1);
**/
//echo "Finished processing the data.  you can return to the <a href=http://corpora.lancs.ac.uk/vocab/analyse_morph.php > main page. </a><br/>"; 



?>