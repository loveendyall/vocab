<?php session_start(); ?>
<!DOCTYPE HTML>
<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>
			English vocabulary and the new-GSL
		</title>
		<link rel="Index"         href="http://corpora.lancs.ac.uk/vocab/"     />
		<link  rel="stylesheet"   href="vocab1.css" type="text/css" media="all" />
		<link rel="shortcut icon" href="http://corpora.lancs.ac.uk/vocab/favicon.ico" />
		<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900,200italic,300italic,400italic,600italic,700italic,900italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
			<script src="clientside/show.js"></script>	
	
	
	</head>
<?php include 'CODE/functions.php'; ?>
<body>	
<div id="container">
		<?php header_page() ?>
	<div id="body">
	

			<!-- Body start -->
			<h1 id="heading">Results: individual items</h1>
			<h2 id="subheading">English vocabulary interactive resource</h2>
			<hr/>
			<a name ="First"></a>
			<p> <strong>First 500 words </strong> </p>
			<p><?php echo !empty($_SESSION['a']) ? display_individual_items($_SESSION['a']) : 'No items were found.';?> </p>
			<hr/>
			<a name ="Second"></a>
			<p> <strong>500 - 1000 words </strong> </p>
			<p><?php echo !empty($_SESSION['b']) ? display_individual_items($_SESSION['b']) : 'No items were found.';?> </p>
			<hr/>
			<a name ="Third"></a>
			<p> <strong>1000- 2500 words </strong> </p>
			<p><?php echo !empty($_SESSION['c']) ? display_individual_items($_SESSION['c']) : 'No items were found.';?> </p>
			<hr/>
			<a name ="Last"></a>
			<p> <strong>Not in the <em>new-GSL</em> </strong> </p>
			<p><?php echo !empty($_SESSION['d']) ? display_individual_items($_SESSION['d']) : 'No items were found.';?> </p>
			
			<br/>
			<!-- Body end -->
		
				
		
		
	</div>
	<div id="footer">
		<!-- Footer start -->
		<p> <?php footer() ?></a> </p>
		<!-- Footer end -->
	</div>
</div>
</body>