<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>
			LancsLex: Lancaster vocab tool
		</title>
		<link rel="Index"         href="http://corpora.lancs.ac.uk/vocab/"     />
		<link  rel="stylesheet"   href="vocab1.css" type="text/css" media="all" />
		<link rel="shortcut icon" href="http://corpora.lancs.ac.uk/vocab/favicon.ico" />
		<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900,200italic,300italic,400italic,600italic,700italic,900italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-49452093-2', 'lancs.ac.uk');
		  ga('send', 'pageview');

		</script>
	
	
	</head>
<?php include 'CODE/functions.php'; ?>
<body>	
<div id="container">
		<?php header_page() ?>
	<div id="body">
		<!-- Body start -->
		<h1 id="heading">LancsLex: Lancaster Vocab Analysis Tool</h1>
		<h2 id="subheading">English vocabulary interactive resource</h2>
		<hr/>
		<p>LancsLex is a lexical tool that analyses lexical coverage of texts and compares it to the New General Service List that identifies 2,490 most frequent words in the English language. The tool provides insights into lexical diversity and complexity of texts and can be used for research as well as for pedagogically-oriented explorations. With the tool you can:</p>
		<ul>
			<li>Identify 2,500 most frequent English words in your text</li>
			<li>Identify specialised vocabulary</li>
			<li>Check lexical appropriateness of teaching materials</li>
			<li>Measure lexical complexity of texts for research purposes</li>
		</ul>
		<p>There are two main ways to use the website:</p>
		<p><strong>1. Explore the New General Service List.</strong> You can explore the general and most frequent words in the English language by browsing the New General Service List (new-GSL). You can also download the whole list ordered according to frequency or alphabetically. Use the ‘Browse’ tab in the top panel to learn more about the new-GSL. </p>

		<p><strong>2. Analyse a text.</strong> You can explore the lexical composition of texts of your own choice by pasting the text on the online interface. The results will tell you how many different words are contained in the text and which of these words came from the New-GSL. You can thus distinguish between general and specialised vocabulary in texts. The website also provides information about the percentage of the text covered by words from different frequency bands. Use the ‘Analyse’ tab in the top panel to use this functionality.</p> 
		<hr/>
		<br/>
		<h2>More about the New General Service List</h2>
		<p>The new-GSL is a list of ~2,500 common English vocabulary items based on four language corpora of the total size of over 12 billion running words. It can be used for both teaching and research purposes. The list differs from other lists of core English vocabulary in three key respects: </p>
		<ul>
			<li>Its innovative use of corpus methods to identify the words in the English language that are truly general, that is, they will be encountered frequently across different genres and topics.</li> 
			<li>It is based on four corpora while the other lists are based on one corpus.</li>
			<li>It is based on lemma as the organising principles. Lemmas include the word and all its inflectional suffixes (e.g. go-goes-going-went) and distinguish between word classes (‘go’ as a verb and ‘go’ as a noun).</li>
		</ul>
		<p>To read more about the development of the New General Service List and about general principles of wordlist creation see the following two papers:</p>
		<ul>
			<li>Brezina, V. & Gablasova, D. (2015). <a href="https://academic.oup.com/applij/article/36/1/1/226623" target="_blank">Is there a core general vocabulary? Introducing the New General Service List</a>. <em>Applied Linguistics 36</em> (1):1-22.</li>
			<li>Brezina, V. & Gablasova, D. (2017). <a href="https://academic.oup.com/applij/article/doi/10.1093/applin/amx022/4036261/How-to-Produce-Vocabulary-Lists-Issues-of" target="_blank"> How to Produce Vocabulary Lists? Issues of Definition, Selection and Pedagogical Aims. A Response to Gabriele Stein</a>. <em>Applied Linguistics, 38</em> (5), 764–767.</li> 
		</ul>
			<br/>
			
		
			<h2>Watch a video about the development of the new-GSL </h2>
			<iframe width="560" height="315" src="//www.youtube.com/embed/UDSqTsEPziU?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
		
		<hr/>
		<br/>
		<h2>More about the Online text analysis tool</h2>
		<p>The tool was designed to allow exploration of lexical composition of English texts. The tool differs from other lexical analysis tools in several major areas.</p>

		<p><strong>1. Identification of word classes.</strong> The tool analyses the texts grammatically which enables the tool to distinguish disambiguate between words with the same form but from a different word class (e.g. ‘walk’ as a noun and ‘walk’ as a verb).</p>

		<p><strong>2. Identification of lemmas.</strong>  The grammatical analysis allows identifying lemmas in the text. In other words, the tool finds all instances of the same word regardless of the different morphological suffices and groups them all together. For example, both singular and plural occurrences of the same word will be recognised as belonging to the same word.</p>

		<p><strong>3. Variety of English.</strong>  It allows to take into consideration whether the text is from American or British variety of English. The British variety is the default, to activate the comparison with the most frequent words in the American English, tick the ‘American supplement’ box. </p>

		<p><strong>4. Inclusion of proper nouns and numbers. </strong>  It allows you to decide whether you want proper nouns and numbers included in the calculation of text coverage. The majority of lexical coverage tools include these words by default; however, it has been argued that inclusion of proper nouns can lead to over-estimation of lexical diversity and sophistication of text and is not preferred in some instances. </p>

		<p>The tool produces information about the number of words in the text and the percentage of the text covered by the New-GSL. This allows users to distinguish between general and specialised vocabulary in the text. In addition, it offers a further breakdown of the lexical items found in the new-GSL into more specific frequency based (e.g. 0-500, 501-1000, etc). It provides the information both in a form of a table as well as in graphic format where words from different frequency bands are highlighted in the text. The tool also provides the frequency breakdown and text coverage according to word classes (e.g. 20 per cent of the text consists of nouns). </p> 


		
		
		<!-- Body end -->
	</div>
	<div id="footer">
		<!-- Footer start -->
		<p> <?php footer() ?></a> </p>
		<!-- Footer end -->
	</div>
</div>
</body>