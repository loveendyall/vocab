import { Injectable } from '@angular/core';

import { HttpClient, HttpResponse } from '@angular/common/http';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../environments/environment';

@Injectable({
	providedIn: 'root'
})
export class ApiService {
	
	public analysisSet: boolean = false;
	public analysis: any;
	
	constructor(private http: HttpClient) { }
	
	search(word: string, categorySearch: string, semCategory: string, wordClass: string): Observable<HttpResponse<any>> {
		word = word ? 'q=' + word : '';
		if (word != '') {
			return this.http.get<HttpResponse<any>>(environment.API + environment.search + '?' + word);
		} else {
			categorySearch = categorySearch ? 'c=' + categorySearch : '';
			semCategory = semCategory ? '&u=' + semCategory : '';
			wordClass = wordClass ? '&p=' + wordClass : '';
			let queryString: string = '?' + categorySearch + semCategory + wordClass;
			return this.http.get<HttpResponse<any>>(environment.API + environment.search + queryString);
		}
	}
	
	analyse(text: string, nounFlag: boolean, numbersFlag: boolean, americanFlag: boolean) {
		let body: any = {};
		body['data'] = text;
		nounFlag ? body['proper'] = true : '';
		numbersFlag ? body['number'] = true : '';
		americanFlag ? body['american'] = true : '';
		return this.http.post(environment.API + environment.analyse, body).pipe(map((response: any) => {
			console.log(response);
			return response;
		}));
	}
}