<?php

include 'functions_morph.php';


$periphrastic = 0;
$segments = 10;
$trials = 100;

set_time_limit(3000);

	
if (isset ($_POST['segments']))
	$segments = (int)$_POST['segments'];
if (isset ($_POST['trials']))
	$trials = (int)$_POST['trials'];

if (!isset($_POST['morphemes_V'])){
	echo "ERROR: Your input could not be processed because the request was badly-formed, please go back and try again.";
	exit;
}
if (!isset($_POST['morphemes_N'])){
	echo "ERROR: Your input could not be processed because the request was badly-formed, please go back and try again.";
	exit;
}

//check the allowed range
if ($trials>1000 | $trials <1)
	$trials = 100;
if ($segments>100 | $segments <5)
	$segments = 10;

/*
if (!isset ($_SESSION['i'])){
	echo "ERROR: Your input could not be processed because the request was badly-formed, please go back and try again.";
	exit;
}
*/

if($periphrastic == 1)
	$periphrastic = "checked";
$retval = utf8_encode(preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;",urldecode($_POST['morphemes_V'])) );
$retval1 = utf8_encode(preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;",urldecode($_POST['morphemes_N'])) );

if ($trials == 1)
	$trial_disp = "trial";
else
	$trial_disp = "trials";

sanitise_morph_box($retval);

$morphology_V = explode(",", $retval);
$morphology_N = explode(",", $retval1);
//trim white space
$morphology_V = array_map('trim',$morphology_V);
$morphology_N = array_map('trim',$morphology_N);



//print_r($morphology_V);


//$morphology_V = array ("ound", "X", "X", "X", "is", "X", "X", "X", "X", "ed", "fell", "was", "was", "X", "ound", "ound", "was", "X", "ound", "made", "fell", "are", "X", "fell", "X", "ed", "came", "was", "fell", "was", "ound", "X", "was", "X", "X");

$morphology_V_diplay = calculate_complexity($morphology_V, $segments, $trials);
$morphology_N_diplay = calculate_complexity($morphology_N, $segments, $trials);
//print_r($morphology_V);

///display starts here

$mysform1 = <<<EOT
    <form name="Form1" action="" method="post" accept-charset="UTF-8" onsubmit="xmlhttpPost('CODE/calculate.php', 'Form1', 'Result1', '<img src=\'style/loading.gif\'>'); return false;">
	<p><strong>All verb exponences in text: </strong></p>
		<textarea rows="5" style="width:100%" name="morphemes_V">
EOT;

$mysform2 = <<<EOT
	</textarea>
			
EOT;

$mysform3 = <<<EOT
	<br/>
	<br/>
	<p> <font color = "blue"> NOUNS </font></p>
				<p><strong>All noun exponences in text: </strong></p>
		
							
				<textarea rows="5" style="width:100%" name="morphemes_N">
EOT;

$mysform4 = <<<EOT
</textarea>
				<br/>
				<hr/>
			<p> <font color = "blue"> PARAMETERS </font></p>
				
					
				
				segment size <input type="number" name="segments" size = "2" min="5" max="100" value = "$segments"> 
				random trials <input type="number" name="trials" size = "2" min="1" max="1000" value = "$trials"> 
				<br/>
				<br/>
				
				<input type="submit" value="Calculate MCI" />
				<br/>
			
			</form>
EOT;

					
echo$mysform1.implode(", ", $morphology_V).$mysform2;
echo$mysform3.implode(", ", $morphology_N).$mysform4;
			
echo'
				<br/>
				<hr/>
			<p> <font color = "blue"> RESULTS </font></p>';
			
echo '<p><strong>Morphological complexity - VERBS: </strong> <font color="blue">('.round($morphology_V_diplay[0],2).' + '.round($morphology_V_diplay[1],2).'/2) - 1 = '.round(($morphology_V_diplay[0] + ($morphology_V_diplay[1]/2)-1),2).'</font></p>';

echo '<p><strong>Morphological complexity - VERBS ('.$trials.' randomised '.$trial_disp.'): </strong> <font color="blue"> ('.round($morphology_V_diplay[7][0],2).' + '.round($morphology_V_diplay[7][1],2).'/2) - 1 = '.round(($morphology_V_diplay[7][0] + ($morphology_V_diplay[7][1]/2)-1),2).'</font></p>
			<br/>';




echo '<p><strong>Morphological complexity - NOUNS: </strong> <font color="blue">('.round($morphology_N_diplay[0],2).' + '.round($morphology_N_diplay[1],2).'/2) - 1 = '.round(($morphology_N_diplay[0] + ($morphology_N_diplay[1]/2)-1),2).'</font></p>';

echo '<p><strong>Morphological complexity - NOUNS ('.$trials.' randomised '.$trial_disp.'): </strong> <font color="blue"> ('.round($morphology_N_diplay[7][0],2).' + '.round($morphology_N_diplay[7][1],2).'/2) - 1 = '.round(($morphology_N_diplay[7][0] + ($morphology_N_diplay[7][1]/2)-1),2).'</font></p>
			<br/>';






?>