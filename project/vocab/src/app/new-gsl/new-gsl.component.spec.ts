import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewGslComponent } from './new-gsl.component';

describe('NewGslComponent', () => {
  let component: NewGslComponent;
  let fixture: ComponentFixture<NewGslComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewGslComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewGslComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
