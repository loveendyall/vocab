<?php
ini_set('display_errors', '1');
chdir('CODE');

include 'functions_morph.php';
include 'functions.php';


if (function_exists('mb_strlen'))
{
	mb_internal_encoding("UTF-8");
	$len = 'mb_strlen';
	$sub = 'mb_substr';
}
else
{
	$len = 'strlen';
	$sub = 'substr';
}

//Is data passed via $_POST?
if (!isset($_POST['data']))
{
	echo "ERROR: Your text could not be processed because the request was badly-formed, please try again.";
	exit;
}


$data = $_POST['data'];

$languages = array('english','french','german', 'italian', 'spanish');
$proper = 0;
$language = "english";
$periphrastic = 0;
$dialog = "";

if (isset ($_POST['proper']))
	$proper = (int)$_POST['proper'];
if (isset($_POST['language'])){
	if (in_array($_POST['language'], $languages))
		$language = $_POST['language'];
}
if (isset ($_POST['periphrastic']))
	$periphrastic = (int)$_POST['periphrastic'];




include 'CODE/language_setup.php';

if($periphrastic ==1 && !in_array($language, $periphrastic_languages)){
	$periphrastic = 3;
}

if($proper ==1 && !in_array($language, $proper_languages)){
	$proper = 3;
}

//Check the length of the input & trim
if ($len($data) > 500000)
{
	$i = 500000;
	while ( false === strpos(" \t\r\n", $sub($data, $i, 1)))
		$i--;
	$data = $sub($data, 0, $i);
}

//Pre-process data
$data = str_replace("\r\n", "\n", $data);
$data = str_replace("»", " »", $data);
$data = str_replace("«", "« ", $data);
$data = str_replace("*", " * ", $data);
$data = str_replace("’", " ' ", $data);
$data = str_replace("‘", " ' ", $data);
$data = str_replace("'", " ' ", $data);
$data = str_replace("”", " \"", $data);
$data = str_replace("“", " \"", $data);
$data = str_replace("»", " »", $data);
//Start tagging
$in = tempnam('/tmp', 'WebTreeTag');
file_put_contents($in, $data);
$out = "$in.vrt";
system("TreeTagger $language $in");



if (false !== strpos($_SERVER['HTTP_USER_AGENT'], 'Windows'))
	system("unix2dos $out");



//Process after tagging and create a new temp file

$inx = tempnam('/tmp', 'processed_file');
$inx1 = "$inx.vrt";
$source= fopen ($out, "r");
//$output= fopen ($inx1, "w");

/*
while (false!==($line=fgets($source))) {
	$new_line = process($line, $proper, $number);
	fwrite($output,$new_line);
}
fclose ($source);
fclose ($output);

/*
//local run	

//set_time_limit(5000);
$inx1 = "test.vrt";

$out = "test.vrt";
$inx = 'N_'.$out;
$source= fopen ("test.vrt", "r");
$output= fopen ($inx, "w");


//end local

*/



///NEW MORPHOLOGY
//create an array with TT output - one value per line
$text = array();
while (false!==($line=fgets($source)))
	$text[] = $line;

fclose($source);

//proper nouns switch
if ($proper == 1)
	$var = '[VMN](?!(P|U))';
else
	$var = '[VMN](?!U)';
//print_r($text);

//further process the text array
$inserted = array();
foreach($text as $key=> &$w){
	//deal with sentence breaks - change them to [S_START] and [S_END]
	if (preg_match('/^<s>\s?$/', $w))
		$w = preg_replace('/^<s>\s?$/','[S_START]', $w);
	if (preg_match('/^<\/s>\s?$/', $w))
		$w = preg_replace('/^<\/s>\s?$/','[S_END]', $w);

	//correct TT errors

	foreach($tterrors as $value){
		//echo "VALUE1: $value[0]";
		if (preg_match($value[0], $w) AND preg_match($value[1], $text[$key-1]) AND preg_match($value[2], $text[$key+1])){
			$w = preg_replace($value[0],$value[3], $w);
			//echo $w;
			//create array with positions and additions
			if($value[4] == 1){
				$inserted[] = array($key, $value[5]);
			}
		}

	}

}

$i=1;
foreach($inserted as $ins){
	array_splice($text, $ins[0]+$i, 0, $ins[1]);
	$i++;
}
//print_r($text);

foreach($text as $key=> &$w){
	//deal with morphology
	if (preg_match('/^.*?\t'.$var.'.*?\t.*?\s?$/', $w)){
		$w1 = preg_replace('/(^.*?\t[VMN].*?\t.*?)\r?\n?$/','$1', $w);
		$w = $w1."\t".analyze_morphology($w, $stem, $irregularities, $umlaut, $umlauts);

		//deal with dictionary 1
		$w = preg_replace($patterns_d1,$replacements_d1, $w);

	}
}


//print_r($text);



if($periphrastic ==1){
//loop through the text array and identify periphrastic morphemes
	foreach($text as $key => &$value){
		//echo "<br/>$value";
		//loop through $array_per1 where	
		for($x = 0; $x < count($array_per1); $x++){
			if (preg_match("/".$array_per1[$x]."/", $value)){
				$count=1;
				//echo"VALUE: $value<br/>";

				//echo $text[$key+$count];
				//stop looping through the array when one of the following conditions is met: 1) end of sentence reached 2) 1000  words passed - to stop infinite loop 3) end morpheme as defined in $array_per2 reached

				while(! preg_match('/\[S_END\]/', $text[$key+$count]) AND ! preg_match($end_limit, $text[$key+$count]) AND $count < $count_max AND ! preg_match("/".$array_per2[$x]."/", $text[$key+$count])){

					$count++;
				}

				//check if condition 3) was reached
				if(preg_match("/".$array_per2[$x]."/", $text[$key+$count])){

					// check if middle morpheme as defined in $array_per3 is present
					if(!empty($array_per3[$x])){
						//try to find the third morpheme between the limits if it exists

						$countinside = $count -1;

						while(! preg_match("/".$array_per3[$x]."/", $text[$key+$countinside]) AND $countinside > 0){
							//echo"COUNTINSIDE: $countinside; VALUE:".$text[$key+$countinside] ."<br/>";
							$countinside--;

						}

						if(preg_match("/".$array_per3[$x]."/", $text[$key+$countinside])){
							//echo "Three found".$text[$key+$countinside];
							$value =  preg_replace($patterns_d2, $replacements_d2,$value);
							$morph1 =  preg_replace('/^(.+?\t.+?\t.+?\t)(.+?)$/','$2', $value);
							//echo $morph1;
							$morph2 =  preg_replace('/^(.+?\t.+?\t.+?\t)(.+?)$/','$2', $text[$key+$count]);
							//echo $morph2;
							$morph3 =  preg_replace('/^(.+?\t.+?\t.+?\t)(.+?)$/','$2', $text[$key+$countinside]);
							$value = preg_replace("/^(.+?\t.+?\t.+?\t)(.+?)$/","$1$morph1-$morph3-$morph2", $value);
							$text[$key+$count] = preg_replace('/^(.+?\t)(.+?)(\t.+?\t).+?$/','$1$2#$3', $text[$key+$count]);
							$text[$key+$countinside] = preg_replace('/^(.+?\t)(.+?)(\t.+?\t).+?$/','$1$2#$3', $text[$key+$countinside]);
						}
						else{
							//echo "Three NOT found".$text[$key+$countinside];
							$value =  preg_replace($patterns_d2, $replacements_d2,$value);
							$morph1 =  preg_replace('/^(.+?\t.+?\t.+?\t)(.+?)$/','$2', $value);
							//echo $morph1;
							$morph2 =  preg_replace('/^(.+?\t.+?\t.+?\t)(.+?)$/','$2', $text[$key+$count]);
							//echo $morph2;
							$value = preg_replace("/^(.+?\t.+?\t.+?\t)(.+?)$/","$1$morph1-$morph2", $value);
							$text[$key+$count] = preg_replace('/^(.+?\t)(.+?)(\t.+?\t).+?$/','$1$2#$3', $text[$key+$count]);
						}


					}
					//else work with only two morphemes
					else{
						//echo "ONLY2";
						$value =  preg_replace($patterns_d2, $replacements_d2,$value);
						$morph1 =  preg_replace('/^(.+?\t.+?\t.+?\t)(.+?)$/','$2', $value);
						//echo $morph1;
						$morph2 =  preg_replace('/^(.+?\t.+?\t.+?\t)(.+?)$/','$2', $text[$key+$count]);
						//echo $morph2;
						$value = preg_replace("/^(.+?\t.+?\t.+?\t)(.+?)$/","$1$morph1-$morph2", $value);
						$text[$key+$count] = preg_replace('/^(.+?\t)(.+?)(\t.+?\t).+?$/','$1$2#$3', $text[$key+$count]);
					}
				}
				//$w1 = preg_replace('/(^.*?\t[VMN].*?\t.*?)\r\n$/','$1', $w);
				//$w = $w1."\t".analyze_morphology1($w)."\r\n";
			}
		}
	}
}


$morphology_V = array();
$morphology_N = array();


//populate morphology arrays

//verbs
foreach($text as &$w){
	if (preg_match('/^.+?\t[MV].*[^#]\t.+?\t.+?\s?$/', $w)){
		$w1 = preg_replace('/^(.+?\t[VM].+?\t.+?\t)(.+?)$/','$2', $w);
		$morphology_V[] = trim($w1);
	}
}

//nouns - proper nouns match the pattern only if the morpheme was added earlier 
foreach($text as &$w){
	if (preg_match('/^.+?\t[N](?!U).+?\t.+?\t.+?\s?$/', $w)){
		$w1 = preg_replace('/^(.+?\t[N].+?\t.+?\t)(.+?)$/','$2', $w);
		$morphology_N[] = trim($w1);
	}
}
/*
foreach($morphology_V as &$w){	
	$w = lump_irregular_verbs($w, $language);
}
foreach($morphology_N as &$w){	
	$w = lump_irregular_nouns($w, $language);
}
*/
//print_r($display_text);

//$morphology_V = array ("ound", "X", "X", "X", "is", "X", "X", "X", "X", "ed", "fell", "was", "was", "X", "ound", "ound", "was", "X", "ound", "made", "fell", "are", "X", "fell", "X", "ed", "came", "was", "fell", "was", "ound", "X", "was", "X", "X");

//calculate complexity for nouns and verbs


//delete temporary files

unlink($in);
unlink($out);
unlink ($inx);
//unlink ($inx1);

/*display the frequency list
foreach ($lexicon as $type => $freq)
	echo '<font color="red">'.$type.'</font> '.$freq.'<br>';
*/
//set 1 as default to avoid division errors



//text array for download
session_start();
$_SESSION['h'] = $text;

//print_r($text);
//$_SESSION['i'] = array($language, $proper, $periphrastic);		 


include 'display_experiment.php';


?>